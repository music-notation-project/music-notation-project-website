\version "2.16.1"

%%%% MNP scripts, version: Feb 9, 2013
%%%% built off of TwinNote Scripts, version: January 14, 2012
%%%% TwinNote scripts end around line 830
%%%% MNP scripts start after line 830

%%%% StaffNumberedNotes added January 8, 2015

%%%% For calculating absolute values
% when LilyPond goes to Guile 2.0 you can just use "abs" without defining it here
#(define (abs x) (if (> x 0) x (- 0 x)))

%%%% Place pitches on the staff
TwinNotePitchLayout = 
#(lambda (p) (floor (/ (+ (ly:pitch-semitones p) 1) 2)))


%%%% Adjust Time Signature position
% apparently not needed anymore, but keeping it around for now
% TwinNoteTimeSignature = 
% #(lambda (grob) (set! (ly:grob-property grob 'Y-offset) -1))


%%%% Custom note head stencils 

upTriangle =
#(ly:make-stencil
	'(path 0.1
		'(
		moveto 	0		-.5
		curveto	0  		-.41
				.453	.4
				0.68  	.4
		curveto	.906	.4
				1.36	-.41
				1.36	-.5
		closepath
		)
		'round  ;; cap
		'round  ;; join
		#t      ;; fill t/f
	)
	(cons 0 1.36)
	(cons -.5 .4)
)

downTriangle =
#(ly:make-stencil
	'(path 0.15
		'(
		moveto 	0 		.5
		curveto	0		.41   
				.453	-.4   
				.68		-.4
		curveto .906	-.4   
				1.36	.41  
				1.36   	.5
		closepath
		moveto	.1		.4
		curveto	.1		.41
				.453	-.4
				.68		-.4
		curveto	.906	-.4
				1.26	.41
				1.26	.4
		closepath
		moveto	.15		.35
		curveto	.15		.35
				.453	-.4
				.68		-.4
		curveto	.906	-.4
				1.16	.35
				1.21	.35
		closepath
		)
		'round
		'round
		#f
	)
	(cons 0 1.36)
	(cons -.4 .5)
)


%%%% TwinNote notehead shapes, size, and stem attachment

% First get needed properties from the notehead grob:
% 1. resizing value (fsz --> mult)
% 2. pitch (ptch) --> pitch in semitones (semi)
% 3. note column (notecol, the parent of notehead grob) --> stem (stm) --> stem direction (stmdir)
% 4. set stem attachment values
 
% NOTEHEADS: assign correct notehad using pitch in semitones, 
% resize based on current font-size using stencil-scale

% STEM ATTACHMENT: Based on semitone pitch (odd or even), 
% is the notehead an up or down triangle, 
% Then based on stem direction, assign stem attachment values


TwinNoteNoteHeads =
#(lambda (grob)
	(let* (
		(fsz  (ly:grob-property grob 'font-size 0.0))
		(mult (magstep fsz))

		(ptch (ly:event-property (event-cause grob) 'pitch))  
		(semi (ly:pitch-semitones ptch))                      

		(notecol (ly:grob-parent   grob    X))
		(stm     (ly:grob-object   notecol 'stem))
		(stmdir  (ly:grob-property stm     'direction))
		
		(upTriUpStem     '(1.08 . -1.1))  
		(upTridownStem   '(1.08 . 1.1))
		(downTriUpStem	 '(1.08 . 1))
		(downTriDownStem '(1.08 . -1.1))
		)
		
		;; NOTEHEADS
		(set! (ly:grob-property grob 'stencil) 
			(ly:stencil-scale 
				(if (= (modulo semi 2) 0) downTriangle upTriangle) 
			mult mult)
		)

		;; STEM ATTACHMENT
		(set! (ly:grob-property grob 'stem-attachment) 
		  	(if (= (modulo semi 2) 1) 
				(if (= UP stmdir) upTriUpStem upTridownStem)				
				(if (= DOWN stmdir) downTriDownStem downTriUpStem)
			)
		)
	)
)


%%%% Half notes get double-stems
% use -.42 or .15 to change the side where the 2nd stem appears

#(define (doubleStemmer grob)
	(if (= 1 (ly:grob-property grob 'duration-log))
		(ly:stencil-combine-at-edge
			(ly:stem::print grob)
			X
			(- (ly:grob-property grob 'direction))
			(ly:stem::print grob)
			-.42 ) 
		(ly:stem::print grob)
	)
)


%%%% TwinNote accidentals

ln = #(ly:make-stencil
  '(path 0.2
    '(moveto .5  .6
      lineto .5 -.6 ))
    (cons .5  .5)
    (cons .6 -.6)
)

crcl = #(make-circle-stencil 0.2 0 #t)

flt = #(ly:stencil-add ln (ly:stencil-translate crcl '(0.5 . -.5)))

shrp = #(ly:stencil-add ln (ly:stencil-translate crcl '(0.5 . .5)))

dblflt = #(ly:stencil-add
	(ly:stencil-translate flt '( -.35 . 0))
	(ly:stencil-translate flt '( .15 . 0))
)

dblshrp = #(ly:stencil-add
	(ly:stencil-translate shrp '( -.35 . 0))
	(ly:stencil-translate shrp '( .15 . 0))
)

barnum = 0
acc-list = #'()

#(define TwinNote_accidental_engraver
  (make-engraver
    (acknowledgers
      ((accidental-interface engraver grob source-engraver)
        (let* (
	    (mult (magstep (ly:grob-property grob 'font-size 0.0)))
	    (acc (accidental-interface::calc-alteration grob))
	
	    (context (ly:translator-context engraver))
	    (curr-barnum (ly:context-property context 'currentBarNumber))
	
	    (note-head (ly:grob-parent grob Y))
		(ptch (ly:event-property (event-cause note-head) 'pitch))  
		(semi (ly:pitch-semitones ptch))
	    )
		
		(cond 
		;; if we're in a new measure, clear the acc-list, and set the barnum
		((not (= barnum curr-barnum)) 
		  (set! acc-list '())
		  (set! barnum curr-barnum)
		))
		
		;; check to see if the accidental is already in effect
		(cond ((equal? (cons semi acc) (assoc semi acc-list))
	      ;; it is in effect, set the stencil to #f to show no accidental
		  (set! (ly:grob-property grob 'stencil) #f)
		))
		(cond ((not (equal? (cons semi acc) (assoc semi acc-list)))
		  ;; it is not in effect, add it to the list and set the stencil
		  (set! acc-list (assoc-set! acc-list semi acc))
		  ;; we don't want naturals that cancel previous accidentals, 
		  ;; so add that natural to the list as well so it is omitted
		  ;; ex: (4 . 1/2) is a sharp, so also add (3 . 0) 
		
		  ;; if the accidental is a natural (? . 0) cancelling a sharp in a key sig
		  ;; then use the key-acc-type to add the sharp or flat 
		  ;; that is in the key sig that the natural is altering
		  (if (= acc 0) 
		    (set! acc-list (assoc-set! acc-list (- semi (* -2 key-acc-type)) key-acc-type ))
		    (set! acc-list (assoc-set! acc-list (- semi (* 2 acc)) 0 ))
		  )
		
		  ;; set the stencil
		  (set! (ly:grob-property grob 'stencil) (ly:stencil-scale 
		    (cond
		      ((= acc -1) dblflt)
		      ((= acc -.5) flt)
		      ((= acc 0) (ly:stencil-scale (ly:grob-property grob 'stencil) .65 .65 )) 
		      ((= acc .5) shrp)
		      ((= acc 1) dblshrp)
		      (else (ly:grob-property grob 'stencil)))
		    mult mult))
		))
      )
	))))


%%%% TwinNote Key Signatures

blklft = #(ly:make-stencil
  '(path 0.01 '(
    moveto 0.0  0.0
    lineto 0.5  0.0
    lineto 0.5  2.0
    lineto 0.0  2.0
    lineto 0.0  0.0
    closepath)
	'round 'round #t )
	(cons 0 0.5)
	(cons 0 2.0)
)
whtlft = #(ly:make-stencil
  '(path 0.2 '(
    moveto 0.0  0.0
    lineto 0.5  0.0
    lineto 0.5  2.0
    lineto 0.0  2.0
    lineto 0.0  0.0
    closepath)
	'round 'round #f )
	(cons 0 0.5)
	(cons 0 2.0)
)
blkrt = #(ly:make-stencil
  '(path 0.01 '(
    moveto 0.5  1.5
    lineto 1.0  1.5
    lineto 1.0  4.0
    lineto 0.5  4.0
    lineto 0.5  1.5
    closepath)
	'round 'round #t )
	(cons 0.5 1.0)
	(cons 1.5 4.0)
)
whtrt = #(ly:make-stencil
  '(path 0.2 '(
    moveto 0.5  1.0
    lineto 1.0  1.0
    lineto 1.0  3.5
    lineto 0.5  3.5
    lineto 0.5  1.0
    closepath)
	'round 'round #f )
	(cons 0.5 1.0)
	(cons 1.0 3.5)
)

majblk = #(ly:stencil-add blklft whtrt)
majwht = #(ly:stencil-add whtlft blkrt)

lldgr    = #(ly:make-stencil '(path 0.15 '(moveto -0.7 0 lineto 1.0 0 )) (cons -0.2  1.5) (cons 0 0.15 ) ) 
rldgr    = #(ly:make-stencil '(path 0.15 '(moveto   0  0 lineto 1.7 0 )) (cons  0.5  2.2) (cons 0 0.15 ) )
wldgr    = #(ly:make-stencil '(path 0.15 '(moveto -0.7 0 lineto 1.7 0 )) (cons -0.2  2.2) (cons 0 0.15 ) ) 

tonic-dot-wht = #(ly:stencil-scale downTriangle .8 .8)
tonic-dot-blk = #(ly:stencil-scale upTriangle .8 .8)

top-line-stncl = #(ly:make-stencil '(path 0.05 '(moveto 0.49 2 lineto 0.5 2 )) (cons 0.49  0.5) (cons 2.1 2))

% Variables 
% tonic-num: number of the tonic note 0-6, C=0, B=6
% acc-count: the number of accidentals in the key signature
% key-acc-type: the accidental sign type, 1/2=sharp, -1/2=flat
% tonic-acc: #f if the tonic note is not sharp or flat, otherwise a pair
% maj-num: number of the tonic note 0-6, if the key sig were major
% sig-type: position of black or white notes: 0=starts with white, 1=starts with black
% mode-num: number of the mode 0-6
% tonic-psn: vertical position of the tonic/mode indicator within the key sig
% tonic-lr: whether the tonic indicator is left or right of the sig, -1.2=left 1.2=right

% key-acc-type is shared between TwinNote_key_signature_engraver and 
% TwinNote_accidental_engraver
key-acc-type = 0

#(define TwinNote_key_signature_engraver
  (make-engraver
    (acknowledgers
      ((key-signature-interface engraver grob source-engraver)
        (let* (
          (grob-name (lambda (x) (assq-ref (ly:grob-property x 'meta) 'name)))
          (context (ly:translator-context engraver))
          (tonic-pitch (ly:context-property context 'tonic))
          (tonic-num (ly:pitch-notename tonic-pitch))
          
          (acc-list (ly:grob-property grob 'alteration-alist))
          (acc-count (length acc-list))
          
          (maj-num 0) 
          (sig-type 1)
          (mode-num 0) 
          (tonic-psn 0) 
          (tonic-lr -1.2)
          (tonic-dot tonic-dot-wht)

          (vert-adj 0)
          (sig majblk)
          (sigldgr #f)
          (top-line top-line-stncl)
          
		  (mult (magstep (ly:grob-property grob 'font-size 0.0)))
          (bble (ly:stencil-translate 
            (ly:stencil-scale 
              (grob-interpret-markup grob 
                (markup (number->string (length acc-list)))
              ) 
            0.6 0.6)
            (cons 0 -0.4))
            )
      )
      (if (null? acc-list) 
        (set! key-acc-type  0) 
        (set! key-acc-type (cdr (list-ref acc-list 0)))
      )
      (set! bble (ly:stencil-combine-at-edge
          (cond 
            ((= key-acc-type 1/2) (ly:stencil-translate shrp (cons -0.5 0)))
            ((= key-acc-type -1/2) (ly:stencil-translate flt (cons -0.5 0))) 
            ((= key-acc-type 0) 
              (ly:stencil-scale (grob-interpret-markup grob (markup #:natural)) 0.65 0.65))
          )
        0 1 bble 0.4
      ))

      ;; print nothing for key cancellations
      (cond 
        ((eq? 'KeyCancellation (grob-name grob)) (ly:grob-set-property! grob 'stencil #f))
        (else 

        ;; set settings based on key-acc-type and acc-count
        (cond 
        ((= key-acc-type 0) 
          (set! maj-num 0) (set! vert-adj -3.5) (set! sig-type 0) (set! sigldgr rldgr))
        ((= key-acc-type 1/2) 
          (cond 
          ((= acc-count 1) (set! maj-num 4) (set! vert-adj -1.5) (set! sigldgr wldgr))
          ((= acc-count 2) (set! maj-num 1) (set! vert-adj -3.0) (set! sig-type 0))
          ((= acc-count 3) (set! maj-num 5) (set! vert-adj -1.0))
          ((= acc-count 4) (set! maj-num 2) (set! vert-adj -2.5) (set! sig-type 0))
          ((= acc-count 5) (set! maj-num 6) (set! vert-adj -0.5) (set! sigldgr lldgr))
          ((= acc-count 6) (set! maj-num 3) (set! vert-adj -2.0) (set! sig-type 0) (set! sigldgr rldgr))
          ((= acc-count 7) (set! maj-num 0) (set! vert-adj -3.0) (set! sigldgr rldgr))
          ))
        ((= key-acc-type -1/2)
          (cond
          ((= acc-count 1) (set! maj-num 3) (set! vert-adj -2.0))
          ((= acc-count 2) (set! maj-num 6) (set! vert-adj -1.0) (set! sig-type 0))
          ((= acc-count 3) (set! maj-num 2) (set! vert-adj -2.5))
          ((= acc-count 4) (set! maj-num 5) (set! vert-adj -1.5) (set! sig-type 0) (set! sigldgr lldgr))
          ((= acc-count 5) (set! maj-num 1) (set! vert-adj -3.0) (set! sigldgr rldgr))
          ((= acc-count 6) (set! maj-num 4) (set! vert-adj -2.0) (set! sig-type 0) (set! sigldgr rldgr))
          ((= acc-count 7) (set! maj-num 0) (set! vert-adj -0.5) (set! sigldgr lldgr))
          )))	
      
        ;; calculate the mode number
        (set! mode-num (modulo (- tonic-num maj-num) 7))
      
        ;; set the position coordinates for the tonic indicator dot
        (cond 
          ((= mode-num 0) (set! tonic-psn -1))   ;; ionian, major
          ((= mode-num 1) (set! tonic-psn -1.5)) ;; dorian
          ((= mode-num 2) (set! tonic-psn -2))   ;; phrygian
          ((= mode-num 3) (set! tonic-psn -2.5)) ;; lydian
          ((= mode-num 4) (set! tonic-psn -3))   ;; mixolydian
          ((= mode-num 5) (set! tonic-psn -3.5)) ;; aeolian, minor
          ((= mode-num 6) (set! tonic-psn -4))   ;; locrian
        )
        (cond ((> mode-num 2) (set! tonic-lr 1.1) 
          (cond ((= sig-type 1) (set! tonic-psn (+ 0.5 tonic-psn)) ))))
        
        ;; which kind of triangle for the tonic dot
        (cond 
          ((and (< mode-num 3) (= sig-type 1)) (set! tonic-dot tonic-dot-blk) (set! tonic-psn (+ 0.1 tonic-psn)))
          ((and (> mode-num 2) (= sig-type 0)) (set! tonic-dot tonic-dot-blk) (set! tonic-psn (+ 0.1 tonic-psn))))

        ;; which sig-type: majwht or majblk
        (if (= sig-type 0) (set! sig majwht))

        ;; add tonic indicator dot to sig
        (set! sig (ly:stencil-combine-at-edge sig 1 -1 
            (ly:stencil-translate tonic-dot (cons tonic-lr 0))
            (+ 0.1 tonic-psn)))

        ;; (newline)(display sigldgr)
        ;; start with top-line marker and add sigldgr line
        (set! top-line (ly:stencil-combine-at-edge top-line 1 1 sigldgr 0))

        ;; add extra sigldgr line for Cmaj/Amin sig
        (cond ((= key-acc-type 0) 
          (set! top-line (ly:stencil-combine-at-edge top-line 1 1 lldgr -5 ))))

        ;; position the sig
        (set! sig (ly:stencil-translate sig (cons 0 vert-adj)))
        
        ;; add the sig
        (set! sig (ly:stencil-add sig top-line))

        ;; add the acc type and count to top
        (set! sig (ly:stencil-combine-at-edge (ly:stencil-translate sig (cons 0 0)) 1  1 bble 0.6))
      
        ;; shift the whole sig to the right for proper spacing with clef
        (cond ((> mode-num 2) 
          (set! sig (ly:stencil-translate sig (cons 0.35 0))))
          (else (set! sig (ly:stencil-translate sig (cons 0.9 0)))))

        ;; resize per current font size and set it
	    (ly:grob-set-property! grob 'stencil (ly:stencil-scale sig mult mult))
      
)))))))
      

%%%% TwinNote Clef Engraver

clef-minus-one = #(markup #:bold #:magnify 0.63 "-1")
clef-minus-two = #(markup #:bold #:magnify 0.63 "-2")

#(define TwinNote_clef_engraver
  (make-engraver
    (acknowledgers
      ((clef-interface engraver grob source-engraver)
        (let* (
          (glyph-name (ly:grob-property grob 'glyph-name))
          (mult (magstep (ly:grob-property grob 'font-size 0.0))))
          
          ;; for dev work
          ;; (newline)(display glyph-name)
          
          (set! (ly:grob-property grob 'stencil)
            (cond
              ((equal? glyph-name "clefs.G")
                (ly:font-get-glyph (ly:grob-default-font grob) "clefs.G_change"))
                
              ((or (equal? glyph-name "clefs.F_change") (equal? glyph-name "clefs.F"))
                (ly:stencil-combine-at-edge 
                  (ly:stencil-translate (ly:font-get-glyph (ly:grob-default-font grob) "clefs.G_change") (cons 0 -2))
                  1 -1 
                  (ly:stencil-translate (grob-interpret-markup grob clef-minus-two) (cons 0.2 0))
                  0.08 ))
                  
              ((or (equal? glyph-name "clefs.C_change") (equal? glyph-name "clefs.C")) 
                (ly:stencil-combine-at-edge 
                  (ly:stencil-translate (ly:font-get-glyph (ly:grob-default-font grob) "clefs.G_change") (cons 0 -1))
                  1 -1 
                  (ly:stencil-translate (grob-interpret-markup grob clef-minus-one) (cons 0.2 0))
                  0.08 ))
              
              (else (ly:grob-property grob 'stencil))))
          )))))
          
%{	for larger, regular size clefs, if we ever get them working
	          ;;((equal? glyph-name "clefs.F")
              ;;  (ly:stencil-combine-at-edge 
              ;;    (ly:stencil-translate (ly:font-get-glyph (ly:grob-default-font grob) "clefs.G") (cons 0 -2))
              ;;    1 -1 
              ;;    (ly:stencil-translate (grob-interpret-markup grob clef-fifteen) (cons 0.5 0))
              ;;    0.005 ))
%}


%%%% To automatically place note heads in chords/harmonies 
%%%% on the correct side of stem 
%%%% This is experimental work in progress.

TwinNoteChordHandler =
#(lambda (grob)
  (let* (
    (heads-array (ly:grob-object grob 'note-heads))
    (note-heads (if (ly:grob-array? heads-array) 
        (ly:grob-array->list heads-array) 
        (list 0) )) ;; handles no note-heads in NoteColumn grob (ie rests) 
    (total-heads (length note-heads))
    (head-grob #f)
    (semi 0)
    (stmdir (ly:grob-property (ly:grob-object grob 'stem) 'direction)) ;; 1 is up, -1 is down
    (n 0)
    (k 0)
    (m 0)
    (semi-list '( ) )
    (int-list '( ) )
    (clust-list '( ) )
    (clust-list-length 0)
    (clust-count 0)
    (new-stemsides '( ) ) ;; new, desired note placements (-1 left, 1 right)
    (old-stemsides '( ) ) ;; old, original, default note placements
    (offset-list '( ) ) 
    )  ;; end variable definitions
    
    ;; (newline) (display (ly:grob-property grob 'X-extent))
    ;; (display "clust-list: ") (display clust-list) (newline)
    ;; (display note-heads)
    ;; (display "----------------------") (newline)
    
    ;; single notes don't need offsetting
    (cond ((> total-heads 1) 
	  
        ;;;;;;;;;;;;;;;;;;; GET SEMITONES ;;;;;;;;;;;;;;;;;;;;
	;; iterate through each note of the chord and
        ;; put the semitone of each note in semi-list
        ;; along with its position as entered in the .ly input file
	(while (< n total-heads) 
	  (set! head-grob (list-ref note-heads n))
          (set! semi (ly:pitch-semitones (ly:event-property (event-cause head-grob) 'pitch)))       
          
          (set! semi-list (append semi-list (list (list n semi ))))
          
          ;; (set! semi-list (append semi-list (list semi) ))          
          (set! n (+ n 1))
          ) 
        ;; (display "----------------------") (newline)
        ;; (display semi-list) (display " - semi-list") (newline)
        
        ;; sort semi-list by semitone, ascending
        (set! semi-list (sort! semi-list (lambda (a b) (< (list-ref a 1) (list-ref b 1) ))))
        
        ;; (display semi-list) (display " - semi-list sorted") (newline)
        
        ;;;;;;;;;;;;;;;;;;;; GET INTERVALS ;;;;;;;;;;;;;;;;;;;;;
        ;; calculate the intervals between the semitones and put them into int-list
        ;; int-list always starts with a 0 so that its length is the same as total-heads
        (set! n 0)
        (while (< n total-heads)
          (if (= n 0) 
            (set! int-list (append int-list (list 0)))
            (set! int-list (append int-list (list 
                (- (list-ref (list-ref semi-list n) 1) 
                    (list-ref (list-ref semi-list (- n 1)) 1))
            ))))
            (set! n (+ n 1))
            ) 
        ;; (display int-list) (display " - int-list")  (newline)
        
        ;; if there are no 3-semitone intervals, then there is
        ;; no need to offset anything, since standard layout is fine.
        (cond (
          (not (equal? #f (memq 3 int-list)))
          
          ;;;;;;;;;;;;;;;;;;;;;; GET CLUSTER PATTERN ;;;;;;;;;;;;;;;;;;
          ;; since int-list starts with 0, we want to start with n = 1 and
          ;; clust-list starts with a 1 for the first note
          (set! n 1)
          (set! clust-list (list 1))
          (while (< n total-heads)
            (set! k (- (length clust-list) 1))
            ;; (display "k: ") (display k) (newline)
            ;; (display "clust-list: ") (display clust-list) (newline)
            
            (if (> (list-ref int-list n) 3)
              (set! clust-list (append clust-list (list 1)))
              (list-set! clust-list k (+ 1 (list-ref clust-list k ) ))
              )
              (set! n (+ n 1))
            ) 
            ;; (display clust-list) (display " - clust-list") (newline)
         
          ;;;;;;;;;;;;;;;;;;;;;; CALCULATE NEW, DESIRED STEM-SIDE POSITIONS ;;;;;;;;;;;;;;;;;;
          ;; n tracks iteration through clust-list
          ;; k tracks the side of the stem (1 or -1)
          ;; clust-count tracks where you are within a cluster
          ;; m tracks which note of the chord you're on
          
          (set! n 0)
          (set! clust-list-length (length clust-list))
              
          (while (< n clust-list-length)
            (set! clust-count (list-ref clust-list n))
               
            ;; if down-stem and odd-numbered cluster (or single note, since 1 = odd)
            ;; then first/lowest head is on right side of stem
            ;; else first/lowest head is on left side of stem
            (cond 
              ((and (= stmdir -1) (odd? clust-count))
                (set! k 1))
              (else 
                (set! k -1)))
            
            ;; iterate through the cluster and put every
            ;; other note on the opposite side of the stem
            (while (> clust-count 0)
              (set! new-stemsides (append new-stemsides (list (list (list-ref (list-ref semi-list m) 0) k))))
              (set! k (* k -1)) 
              (set! clust-count (- clust-count 1))
              (set! m (+ 1 m))
              )
              (set! n (+ 1 n))
              ) 
          
            ;; (display "----------------------") (newline)
            ;; (display semi-list) (display " - semi-list") (newline)
            ;; (display semi-list) (display " - semi-list sorted") (newline)
            ;; (display int-list) (display " - int-list")  (newline)
            ;; (display clust-list) (display " - clust-list") (newline)
            ;; (display new-stemsides) (display " - new-stemsides") (newline)
            
            ;; sort new-stemsides by position notes were entered in .ly input file
            (set! new-stemsides (sort! new-stemsides (lambda (a b) (< (list-ref a 0) (list-ref b 0) ))))
            ;; (display new-stemsides) (display " - new-stemsides sorted") (newline)
          
          ;;;;;;;;;;;;;;;;;;;;;; GET OLD, DEFAULT STEM-SIDE POSITIONS ;;;;;;;;;;;;;;;;;;
          (set! n 0)
          (while (< n total-heads) 
            (set! head-grob (list-ref note-heads n))
	    (set! k (ly:grob-relative-coordinate head-grob grob 0))
              (cond 
                ((and (= stmdir 1) (= k 0)) (set! old-stemsides (append old-stemsides (list -1)))) ;; left of up-stem
                ((and (= stmdir 1) (> k 0)) (set! old-stemsides (append old-stemsides (list 1)))) ;; right of up-stem                   
                ((and (= stmdir -1) (= k 0)) (set! old-stemsides (append old-stemsides (list 1)))) ;; right of down-stem
                ((and (= stmdir -1) (< k 0)) (set! old-stemsides (append old-stemsides (list -1)))) ;; left of down-stem
              )
              (set! n (+ n 1))
            )
          ;; (display old-stemsides) (display " - old-stemsides") (newline)
          
          ;;;;;;;;;;;;;;;;;;;;;; GENERATE OFFSETS ;;;;;;;;;;;;;;;;;;       
          ;; if old-stemside and new-stemside are the same, 0 is the offset
          ;; otherwise use -1 or 1 from new-stemsides
          
          (set! n 0)
          (while (< n total-heads)
            (if (= (list-ref old-stemsides n) (list-ref (list-ref new-stemsides n) 1))
                (set! offset-list (append offset-list (list 0)))
                (set! offset-list (append offset-list (list (list-ref (list-ref new-stemsides n) 1))))
                )
            (set! n (+ n 1))
            ) 
          ;; (display offset-list) (display " - offset-list") (newline)
          
          ;;;;;;;;; CALL THE MANUAL OFFSET FUNCTION ;;;;;;;;;;;;;
          ;; (check for 1 or -1 in offset-list) no need to send ( 0 0 0 0 0 )  
          ;; if there are only zeros in offset-list, there is nothing to offset       
          
          (cond ((not (and 
            (equal? #f (memq 1 offset-list)) 
            (equal? #f (memq -1 offset-list))
            ))                 
            ((shift offset-list) grob)
          ))
          
        )) ;; end check for any 3-semitone intervals
    )) ;; end check for single notes 
))
               
               
%%%% For chords and intervals, manually shift note heads to left or right of stem

#(define ((shift offsets) grob)
"Defines how NoteHeads should be moved according to the given list of offsets."
 (let* (
 ;; NoteHeads
        ;; Get the NoteHeads of the NoteColumn
        (note-heads (ly:grob-array->list (ly:grob-object grob 'note-heads)))
        ;; Get their durations
        (nh-duration-log 
          (map 
            (lambda (note-head-grobs)
              (ly:grob-property note-head-grobs 'duration-log))
            note-heads))
        ;; Get the stencils of the NoteHeads
        (nh-stencils 
          (map 
            (lambda (note-head-grobs)
              (ly:grob-property note-head-grobs 'stencil))
            note-heads))
        ;; Get their length in X-axis-direction
        (stencils-x-lengths 
          (map 
            (lambda (x) 
                (let* ((stencil (ly:grob-property x 'stencil))
                       (stencil-X-exts (ly:stencil-extent stencil X))
                       (stencil-lengths (interval-length stencil-X-exts)))
                stencil-lengths))
             note-heads))
 ;; Stem
        (stem (ly:grob-object grob 'stem))
        (stem-thick (ly:grob-property stem 'thickness 1.3))
        (stem-x-width (/ stem-thick 10))      
        (stem-dir (ly:grob-property stem 'direction))
        
        ;; stencil width method, doesn't work with non-default beams
        ;; so using thickness property above instead
        ;; (stem-stil (ly:grob-property stem 'stencil))
        ;; (stem-x-width (if (ly:stencil? stem-stil)
        ;;                 (interval-length (ly:stencil-extent stem-stil X))
        ;;                 ;; if no stem-stencil use 'thickness-property
        ;;                 (/ stem-thick 10)))
        
        ;; Calculate a value to compensate the stem-extension
        (stem-x-corr 
          (map 
            (lambda (q)
               ;; TODO better coding if (<= log 0)
               (cond ((and (= q 0) (= stem-dir 1))
                      (* -1 (+ 2  (* -4 stem-x-width))))
                     ((and (< q 0) (= stem-dir 1))
                      (* -1 (+ 2  (* -1 stem-x-width))))
                     ((< q 0)
                      (* 2 stem-x-width))
                     (else (/ stem-x-width 2))))
             nh-duration-log)))
   
   ;; (display offsets) (display " - offsets") (newline)
   
 ;; Final Calculation for moving the NoteHeads   
   (for-each
     (lambda (nh nh-x-length off x-corr) 
         (if (= off 0)
           #f 
           (ly:grob-translate-axis! nh (* off (- nh-x-length x-corr)) X)))
     note-heads stencils-x-lengths offsets stem-x-corr)))

% shift note heads
snhs =
#(define-music-function (parser location offsets) (list?)
"
 Moves the NoteHeads, using (shift offsets)
"
 #{
    \once \override NoteColumn #'before-line-breaking = #(shift offsets)
 #})

setOtherScriptParent =
#(define-music-function (parser location which-note-head)(integer?)
"
 If the parent-NoteHead of a Script is moved, another parent from the 
 NoteColumn could be chosen.
 The NoteHeads are numbered 1 2 3 ...
 not 0 1 2 ...
"
#{
        %% Let "staccato" be centered on NoteHead, if Stem 'direction is forced
        %% with \stemUp, \stemDown, \voiceOne, \voiceTwo etc
        \once \override Script #'toward-stem-shift = #0
        
        \once \override Script #'after-line-breaking =
          #(lambda (grob)
             (let* ((note-head (ly:grob-parent grob X))
                    (note-column (ly:grob-parent note-head X))
                    (note-heads-list 
                      (ly:grob-array->list 
                        (ly:grob-object note-column 'note-heads)))
                    (count-note-heads (length note-heads-list)))
             (if (> which-note-head count-note-heads)
               (ly:warning "Can't find specified note-head - ignoring")
               (set! (ly:grob-parent grob X)
                     (list-ref note-heads-list (- which-note-head 1))))))
#})

adjustStem =
#(define-music-function (parser location val)(pair?)
"
 Adjust 'stem-attachment via 
 adding multiples of the stem-width to the x-default (car val)
 and multiplying the y-default with (cdr val).
"
#{
   \once \override NoteHead #'before-line-breaking = 
   #(lambda (grob)
     (let* ((stem-at (ly:grob-property grob 'stem-attachment))
            (stem (ly:grob-object grob 'stem))
            (stem-x-width (interval-length (ly:grob-property stem 'X-extent))))
     (ly:grob-set-property! 
       grob
       'stem-attachment 
       (cons (+ (car stem-at) (* stem-x-width (car val))) (* (cdr val) (cdr stem-at))) 
       )))
#})

%{  OLD VERSION
#(define ((shift offsets) grob)
  (let ((note-heads (ly:grob-array->list (ly:grob-object grob 'note-heads))))
    (for-each
      (lambda (p q)
        (if (< 1.3 (+ (car (ly:grob-property p 'X-extent)) (cdr (ly:grob-property p 'X-extent))))
          (set! (ly:grob-property p 'X-offset) (* q 1.45)))) 
            note-heads offsets))) 

shiftNHs =
#(define-music-function (parser location offsets) (list?)
  #{ \once \override NoteColumn #'before-line-breaking = #(shift offsets) #} )
%}

staffSize = 
#(define-music-function (parser location new-size) (number?)
  #{
    \set Staff.fontSize = #new-size  
    \override Staff.StaffSymbol #'staff-space = #(magstep new-size)
    \override Staff.StaffSymbol #'thickness = #(magstep new-size)
  #})



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MNP SCRIPTS


%%%%%%%%%%%%%%%%%%%%%%%%
% CUSTOM NOTE HEADS
% pattern = 66, 75, or 0 for default noteheads 
% cfill = 0 for white C note, 1 for black C note
% cfill does nothing if pattern is not 66
% midpoint is the middle staff position (staff-line position) 
% this is used to set default stem direction
% above the midpoint of the staff is down
% below the midpoint of the staff is up
%
#(define (mnpNoteHeads pattern cfill midpoint) 
  (lambda (grob)
	(let* (
		(fsz  (ly:grob-property grob 'font-size 0.0))
		(mult (magstep fsz))

		(ptch (ly:event-property (event-cause grob) 'pitch))  
		(semi (ly:pitch-semitones ptch))
               (note-shape 0) 

		(notecol (ly:grob-parent   grob    X))
		(stm     (ly:grob-object   notecol 'stem))
               (ypos (ly:grob-staff-position grob))
		;; (stmdir  (ly:grob-property stm     'direction))
		
               (fn (ly:grob-default-font grob))
               (whitenote (ly:font-get-glyph fn "noteheads.s1"))
               (blacknote (ly:font-get-glyph fn "noteheads.s2"))  
		)
		
		;; NOTEHEADS
              (case pattern
                ((66) ;; 6-6 pattern
                  (set! note-shape (modulo (+ semi cfill) 2))
		    (set! (ly:grob-property grob 'stencil) 
			(ly:stencil-scale 
                         (case note-shape 
                           ((0) whitenote)
                           ((1) blacknote)
                         )
			mult mult)
		    ))
                ((75) ;; 7-5 pattern
                  (set! note-shape (modulo semi 12))
                  (set! (ly:grob-property grob 'stencil) 
			(ly:stencil-scale 
                         (case note-shape 
                           ((0) whitenote)
                           ((1) blacknote)
                           ((2) whitenote)
                           ((3) blacknote)
                           ((4) whitenote)
                           ((5) whitenote)
                           ((6) blacknote)
                           ((7) whitenote)
                           ((8) blacknote)
                           ((9) whitenote)
                           ((10) blacknote)
                           ((11) whitenote)
                         )
			mult mult)
		      )) 
                 )
            ;; DEFAULT STEM DIRECTION
            (if (< ypos midpoint) 
              (set! (ly:grob-property stm 'default-direction) 1)
              (set! (ly:grob-property stm 'default-direction) -1)
            )
	)
))


%%%%%%%%%%%%%%%%
% Twinline 

TwinlinePitchLayout = 
#(lambda (p) (+ 1 (round (+ -1 (/ (ly:pitch-semitones p) 2)))))

% Twinline Triangle note head shapes

TLupTriBlack =
#(ly:make-stencil
	'(path 0.1
		'(
		moveto 	0		-.5
		lineto      0.68  	.4
		lineto	1.36 	-.5
		closepath
		)
		'round  ;; cap
		'round  ;; join
		#t      ;; fill t/f
	)
	(cons 0 1.36)
	(cons -.5 .4)
)

TLdownTriBlack =
#(ly:make-stencil
	'(path 0.1
		'(
		moveto 	0		.5
		lineto      0.68  	-.4
		lineto	1.36 	.5
		closepath
		)
		'round  ;; cap
		'round  ;; join
		#t      ;; fill t/f
	)
	(cons 0 1.36)
	(cons -.4 .5)
)

TLupTriWhite =
#(ly:make-stencil
	'(path 0.2
		'(
		moveto 	0		-.5
		lineto      0.68  	.4
		lineto	1.36 	-.5
		closepath
		)
		'round  ;; cap
		'round  ;; join
		#f      ;; fill t/f
	)
	(cons 0 1.36)
	(cons -.5 .4)
)

TLdownTriWhite =
#(ly:make-stencil
	'(path 0.2
		'(
		moveto 	0		.5
		lineto      0.68  	-.4
		lineto	1.36 	.5
		closepath
		)
		'round  ;; cap
		'round  ;; join
		#f      ;; fill t/f
	)
	(cons 0 1.36)
	(cons -.4 .5)
)

% Twinline note head shapes
% ovals = 1 for black ovals, 0 for white ovals
% triangles = 1 for black triangles, 0 for white triangles
%
#(define (TwinlineNoteHeads ovals triangles)
  (lambda (grob)
	(let* (
		(fsz  (ly:grob-property grob 'font-size 0.0))
		(mult (magstep fsz))

		(ptch (ly:event-property (event-cause grob) 'pitch))  
		(semi (ly:pitch-semitones ptch))
               (note-shape (modulo semi 4))
               
               (fn (ly:grob-default-font grob))
               (whitenote (ly:font-get-glyph fn "noteheads.s1"))
               (blacknote (ly:font-get-glyph fn "noteheads.s2"))  
               
               (upTri (if (= 0 triangles) TLupTriWhite TLupTriBlack))
               (downTri (if (= 0 triangles) TLdownTriWhite TLdownTriBlack))
               (ovls (if (= 0 ovals) whitenote blacknote))

		(notecol (ly:grob-parent   grob    X))
		(stm     (ly:grob-object   notecol 'stem))
		(stmdir  (ly:grob-property stm     'direction))
		
		(upTriUpStem     '(1.08 . -1.1))  
		(upTridownStem   '(1.08 . 1.1))
		(downTriUpStem	 '(1.08 . 1))
		(downTriDownStem '(1.08 . -1.1))
		)
		
		;; NOTEHEADS
		(set! (ly:grob-property grob 'stencil) 
			(ly:stencil-scale 
                         (case note-shape 
                           ((0) ovls)
                           ((1) upTri)
                           ((2) ovls)
                           ((3) downTri))
			mult mult)
		)

		;; STEM ATTACHMENT
              (if (= (modulo semi 2) 1) 
                  (if (= note-shape 1)
                      (set! (ly:grob-property grob 'stem-attachment) 
                        (if (= UP stmdir) upTriUpStem upTridownStem))
                      (set! (ly:grob-property grob 'stem-attachment) 
			  (if (= DOWN stmdir) downTriDownStem downTriUpStem))
                  )
		)
	)
))


%%%%%%%%%%%%%%%%
% Numbered Notes

NumberedNotesNoteHeads =
#(lambda (grob)
	(let* (
		(ptch (ly:event-property (event-cause grob) 'pitch))  
		(semi (ly:pitch-semitones ptch))
		(notecol (ly:grob-parent grob X))
		(lencol (ly:grob-array-length (ly:grob-object notecol 'note-heads)))
		(stm (ly:grob-object notecol 'stem))
		(stmdir (ly:grob-property stm 'direction))
		(dur (ly:grob-property stm 'duration-log))
		(dot (ly:grob-object grob 'dot))
		(dot-count (if (ly:grob? dot) (ly:grob-property dot 'dot-count) 0))
		)
			
		;; NOTEHEADS
		
		(set! (ly:grob-property grob 'stencil)
		  (grob-interpret-markup grob
			(markup #:concat (
			  ; use number from 1 to 12 for note
			  (number->string (+ 1 (modulo semi 12)))
			  ; exponent indicates duration of whole and half notes
			  ; some dots can be included in the exponent and we will
			  ; remove them later; but if there are too many, we
			  ; leave them alone
			  #:super (cond ((= 0 dur) ; whole note
				              (cond ((= dot-count 1) "6")
							        ((= dot-count 2) "7")
							        (else "4")))
						    ((= 1 dur) ; half note
							  (if (= dot-count 1) "3" "2"))
							(else ""))))))

		; remove dots which were included in the exponent
		(if (or (and (= 0 dur) (= dot-count 1))
				(and (= 0 dur) (= dot-count 2))
				(and (= 1 dur) (= dot-count 1)))
			(set! (ly:grob-property dot 'dot-count) 0))

		;; STEM ATTACHMENTS
		
		(if (<= dur 2)
		    (begin
		      ; remove stem for quarter notes and half notes
		      ; transparent removes a few pixels that are still
		      ; being drawn even with the stem length set to 0
			  (set! (ly:grob-property stm 'length) 0)
			  (set! (ly:grob-property stm 'transparent) #t))
 			(set! (ly:grob-property grob 'stem-attachment)
 			  ; tweak stem attachment to make it look better
 			  (cond ((and (= stmdir UP)   (= lencol 1)) '(1.4 . 0.7))
 			        ((and (= stmdir UP)   (> lencol 1)) '(1.4 . 0.7))
 			        ((and (= stmdir DOWN) (= lencol 1)) '(0.8 . -0.7))
 			        ((and (= stmdir DOWN) (> lencol 1)) '(1.3 . -1)))))
 			             
        ; adjust length of upward stems; downward stems seem to be okay
        (if (= stmdir UP)
          (ly:grob-set-property! stm 'details
            (map
              (lambda (detail)
                (let ((head (car detail))
                      (args (cdr detail)))
                  (if (eq? head 'stem-shorten)
                      (cons head args)
                      (cons head
                        (map
                          (lambda (arg) (* arg 1.35))
                          args)))))
              (ly:grob-property stm 'details))))
	)
)


%%%%%%%%%%%%%%%
% Dashed Lines

dashedStaffSymbolLines =
#(define-music-function (parser location dash-space bool-list)
 ((number-pair? '(0.5 . 0.5)) list?)
"
Replaces specified lines of a StaffSymbol with dashed lines.

The lines to be changed should be given as a list containing booleans, with
the meaning:
  #f - no dashes, print a normal line
  #t - print a dashed line
The order of the bool-list corresponds with the order of the given list of
'line-positions or if not specified, with the default.
If the length of the bool-list and the 'line-positions doesn't match a warning
is printed.

The width of the dashes and the spacing between them can be altered by adding a 
pair as first argument while calling the function:
\\dashedStaffSymbolLines #'(1 . 1) #'(#f #t #f)
the first number of the pair is the width, the second the spacing
"
#{
 \override Staff.StaffSymbol #'after-line-breaking =
   #(lambda (grob)
     (let* ((staff-stencil (ly:grob-property grob 'stencil))
            (staff-line-positions (ly:grob-property grob 'line-positions))
            (staff-width
              (interval-length
                (ly:stencil-extent staff-stencil X)))
            (staff-space (ly:staff-symbol-staff-space grob))
            (staff-line-thickness (ly:staff-symbol-line-thickness grob))
            ;; width of the dash
            (dash-width (car dash-space))
            ;; space between dashes
            (space-width (cdr dash-space))
            ;; Construct the first dash
            (sample-path `((moveto 0 0)
                           (lineto ,dash-width 0)
                           ))
            ;; Make a stencil of the first dash
            (dash-stencil
              (grob-interpret-markup
                grob
                (markup
                  #:path staff-line-thickness sample-path)))
           ;; width of both dash and space
           (dash-space-width (+ dash-width space-width))
           
           ;; Or get width of dash from the stencil. Is this needed?
           ;;(stil-width
           ;;  (interval-length
           ;;    (ly:stencil-extent dash-stencil X)))
           ;;(dash-space-width (+ stil-width space-width))
           
            ;; Make a guess how many dashes are needed.
            (count-dashes
              (inexact->exact
                (round
                  (/ staff-width
                     (- dash-space-width
                        staff-line-thickness)))))
            ;; Construct a stencil of dashes with the guessed count
            (dashed-stil
                (ly:stencil-aligned-to
                  (apply ly:stencil-add
                    (map
                      (lambda (x)
                        (ly:stencil-translate-axis
                          dash-stencil
                          (* (- dash-space-width staff-line-thickness) x)
                          X))
                      (iota count-dashes)))
                  Y
                  CENTER))
            ;; Get the the length of that dashed stencil
            (stil-x-length
              (interval-length
                (ly:stencil-extent dashed-stil  X)))
            ;; Construct a line-stencil to replace the staff-lines.
            (line-stil
              (make-line-stencil staff-line-thickness 0 0 staff-width 0))
            ;; Calculate the factor to scale the dashed-stil to fit
            ;; the width of the original staff-symbol-stencil
            (corr-factor
              (/ staff-width (- stil-x-length staff-line-thickness)))
            ;; Construct the new staff-symbol
            (new-stil
              (apply
                ly:stencil-add
                  (map
                    (lambda (x y)
                      (ly:stencil-translate
                          (if (eq? y #f)
                            line-stil
                            (ly:stencil-scale
                              dashed-stil
                              corr-factor 1))
                          (cons (/ staff-line-thickness 2)
                                (* (/ x 2) staff-space))))
                    staff-line-positions bool-list))))
      (if (= (length bool-list)(length staff-line-positions))
        (ly:grob-set-property! grob 'stencil new-stil)
        (ly:warning
          "length of bool-list doesn't fit the line-positions - ignoring"))))
#})


%%%%%%%%%%%%%%%%%%%%%%%
% STAFF DEFINITIONS
% each staff type gets its own set of overrides, etc.

\layout {

  \context {
    \Staff
    \name StaffFiveLineOne
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'( -4 -2 0 2 4 )
    \override Stem #'no-stem-extend = ##t
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \numericTimeSignature
  }  
  
  \context {
    \Staff
    \name StaffFiveLineTwo
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'( -4 -2 0 2 4  8 10 12 14 16)
    \override Stem #'no-stem-extend = ##t
    \override NoteHead #'before-line-breaking = #(mnpNoteHeads 0 0 6)
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \numericTimeSignature
  }  
  
  \context {
    \Staff
    \name StaffParncuttOne
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'( -1 1 3 5 )
    \override Stem #'no-stem-extend = ##t
    \override NoteHead #'before-line-breaking = #(mnpNoteHeads 0 0 2)
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \override TimeSignature #'before-line-breaking = #(lambda (grob) (set! (ly:grob-property grob 'Y-offset) 1.5))
    \numericTimeSignature
  }
  
  \context {
    \Staff
    \name StaffParncuttTwo
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'( -1 1  3 5  11 13 15 17)
    \override StaffSymbol #'ledger-positions = #'(-39 -37 -35 -33 -31 -29 -27 -25 -23 -21 -19 -17 -15 -13 -11 -9 -7 -5  -3 -1 1  3 5 (7 9) 11 13 15 17 19 21 23 25 27 29 31 33 35 37 39)
    \override Stem #'no-stem-extend = ##t
    \override NoteHead #'before-line-breaking = #(mnpNoteHeads 0 0 7.5)
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \override TimeSignature #'before-line-breaking = #(lambda (grob) (set! (ly:grob-property grob 'Y-offset) 1.5))
    \numericTimeSignature
  }
  
  \context {
    \Staff
    \name StaffIsomorphOne
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'(  -4.1 -4 -3.9    0  2  4   7.9 8 8.1 )
    \override StaffSymbol #'ledger-positions = #'( -4 -2 0 2 4)
    \override Stem #'no-stem-extend = ##t
    \override NoteHead #'before-line-breaking = #(mnpNoteHeads 66 1 2)    
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \numericTimeSignature
  }
  
  \context {
    \Staff
    \name StaffIsomorphTwo
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'( -4.1 -4 -3.9   0  2  4   7.9 8 8.1  12 14 16  19.9 20 20.1 )
    \override StaffSymbol #'ledger-positions = #'( -4 -2 0 2 4)
    \override Stem #'no-stem-extend = ##t
    \override NoteHead #'before-line-breaking = #(mnpNoteHeads 66 1 8)    
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \numericTimeSignature
  }  

  \context {
    \Staff
    \name StaffBeyreutherChromaticSixSix
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'( -6 -2  2  6  10  )
    \dashedStaffSymbolLines #'(1.5 . 1.5) #'(#f #f #t #f #f)
    \override StaffSymbol #'ledger-positions = #'( -6  -2  2  6 10  )
    \override Stem #'no-stem-extend = ##t
    \override NoteHead #'before-line-breaking = #(mnpNoteHeads 66 1 2)
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \override TimeSignature #'before-line-breaking = #(lambda (grob) (set! (ly:grob-property grob 'Y-offset) 1))
    \numericTimeSignature
  }  
  
  
  \context {
    \Staff
    \name StaffBeyreutherUntitledOne
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'(  -2  2  )
    \override StaffSymbol #'ledger-positions = #'( -2  2  )
    \override Stem #'no-stem-extend = ##t
    \override NoteHead #'before-line-breaking = #(mnpNoteHeads 66 1 0)
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \override TimeSignature #'before-line-breaking = #(lambda (grob) (set! (ly:grob-property grob 'Y-offset) 1))
    \numericTimeSignature
  }  
  
  \context {
    \Staff
    \name StaffBeyreutherUntitledTwo
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'(  -2  2    10 14 )
    \override StaffSymbol #'ledger-positions = #'( -2  2  6  10 14 )
    \override Stem #'no-stem-extend = ##t
    \override NoteHead #'before-line-breaking = #(mnpNoteHeads 66 1 6)
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \override TimeSignature #'before-line-breaking = #(lambda (grob) (set! (ly:grob-property grob 'Y-offset) 1))
    \numericTimeSignature
  }    
  
  \context {
    \Staff
    \name StaffChromaticLyre
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'(  -2  2    10 14 )
    \override StaffSymbol #'ledger-positions = #'( -2  2  6  10 14 )
    \override Stem #'no-stem-extend = ##t
    \override NoteHead #'before-line-breaking = #(mnpNoteHeads 66 0 6)
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \override TimeSignature #'before-line-breaking = #(lambda (grob) (set! (ly:grob-property grob 'Y-offset) 1))
    \numericTimeSignature
  }  
  
  \context {
    \Staff
    \name StaffMutoOne
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'(  -6.1 -6 -5.9  0  5.9 6 6.1  )
    \override StaffSymbol #'ledger-positions = #'( -6 -4 -2 0 2 4 6 )
    \override Stem #'no-stem-extend = ##t
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \override TimeSignature #'before-line-breaking = #(lambda (grob) (set! (ly:grob-property grob 'Y-offset) 1))
    \numericTimeSignature
  }  
  
  \context {
    \Staff
    \name StaffMutoTwo
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'(  -6.1 -6 -5.9  0  5.9 6 6.1  12 17.9 18 18.1  )
    \override StaffSymbol #'ledger-positions = #'( -6 -4 -2 0 2 4 6 )
    \override Stem #'no-stem-extend = ##t
    \override NoteHead #'before-line-breaking = #(mnpNoteHeads 0 0 6)
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \override TimeSignature #'before-line-breaking = #(lambda (grob) (set! (ly:grob-property grob 'Y-offset) -1))
    \numericTimeSignature
  }  
  
  \context {
    \Staff
    \name StaffKlavarMirckOne
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'( -5 -3  -0.1 0 0.1  1.9 2 2.1  3.9 4 4.1 )
    \override StaffSymbol #'ledger-positions = #'( -5 -3  0 2 4 7)
    \override Stem #'no-stem-extend = ##t
    \override NoteHead #'before-line-breaking = #(mnpNoteHeads 75 0 0)    
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \override TimeSignature #'before-line-breaking = #(lambda (grob) (set! (ly:grob-property grob 'Y-offset) -0.5))
    \numericTimeSignature
  }
  
  \context {
    \Staff
    \name StaffKlavarMirckTwo
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'( -5 -3  -0.1 0 0.1  1.9 2 2.1  3.9 4 4.1   7 9  11.9 12 12.1  13.9 14 14.1  15.9 16 16.1 )
    \override StaffSymbol #'ledger-positions = #'( -5 -3  0 2 4 7)
    \override Stem #'no-stem-extend = ##t
    \override NoteHead #'before-line-breaking = #(mnpNoteHeads 75 0 6)    
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \override TimeSignature #'before-line-breaking = #(lambda (grob) (set! (ly:grob-property grob 'Y-offset) -0.5))
    \numericTimeSignature
  }
  
  \context {
    \Staff
    \name StaffTwinlineReed 
    \alias Staff
    staffLineLayoutFunction = \TwinlinePitchLayout
    \override NoteHead #'before-line-breaking = #(TwinlineNoteHeads 0 0)
    \override StaffSymbol #'line-positions = #'( 4 2 -2 -4 )
    \override Stem #'stencil = #doubleStemmer
    \override NoteColumn #'before-line-breaking = \TwinNoteChordHandler   
    \numericTimeSignature
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }
  
  \context {
    \Staff
    \name StaffTwinlineBlackTriangle
    \alias Staff
    staffLineLayoutFunction = \TwinlinePitchLayout
    \override NoteHead #'before-line-breaking = #(TwinlineNoteHeads 0 1)
    \override StaffSymbol #'line-positions = #'( 4 2 -2 -4 )
    \override Stem #'stencil = #doubleStemmer
    \override NoteColumn #'before-line-breaking = \TwinNoteChordHandler   
    \numericTimeSignature
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }
  
  \context {
    \Staff
    \name StaffTwinlineBlackOval
    \alias Staff
    staffLineLayoutFunction = \TwinlinePitchLayout
    \override NoteHead #'before-line-breaking = #(TwinlineNoteHeads 1 0)
    \override StaffSymbol #'line-positions = #'( 4 2 -2 -4 )
    \override Stem #'stencil = #doubleStemmer
    \override NoteColumn #'before-line-breaking = \TwinNoteChordHandler   
    \numericTimeSignature
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }

  \context {
    \Staff
    \name StaffTwinNote 
    \alias Staff
    \consists \TwinNote_key_signature_engraver
    \consists \TwinNote_clef_engraver
    \consists \TwinNote_accidental_engraver
    printKeyCancellation = ##f
    staffLineLayoutFunction = \TwinNotePitchLayout
    \override NoteHead #'before-line-breaking = \TwinNoteNoteHeads
    \override StaffSymbol #'line-positions = #'( 4 2 -2 -4 )
    \override Stem #'stencil = #doubleStemmer
    \override NoteColumn #'before-line-breaking = \TwinNoteChordHandler   
    \numericTimeSignature
    \remove "Accidental_engraver"
  }

  \context {
    \Staff
    \name StaffNumberedNotes
    \alias Staff
    
    % This is an attempt to implement the system presented at
    % numberednotes.com from 2009 to 2012 and described at:
    %
    % http://qmusiqconsortium.ning.com/profiles/blogs/how-we-are-going-to-raise
    %
    % See also:
    %
    % http://musicnotation.org/wiki/notation-systems/numbered-notes-jason-maccoy/
    
    %%% STAFF %%%
    
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'( -4 -1 2 5 )
    \dashedStaffSymbolLines #'(0.6 . 0.6) #'(#t #f #f #f)
    \override LedgerLineSpanner #'transparent = ##t
    % extra padding for staffs to make things clearer
    \override VerticalAxisGroup.staff-staff-spacing = #'((padding . 3.5))

    %%% CLEF %%%
    
    % replace clef symbol with octave number
    \override Clef #'stencil = #(lambda (grob)
	  (let* (
	    (glyph (ly:grob-property grob 'glyph))
	    (offset (ly:grob-property grob 'Y-offset))
		(octave (cond
		  ; treble clef
		  ((and (string=? glyph "clefs.G") (= offset -1)) "4")
		  ; bass clef
		  ((and (string=? glyph "clefs.F") (= offset 1)) "3")
		  ; TO DO: handle other clefs
		  (else #f))))
	    (if octave (grob-interpret-markup grob (markup #:bold #:fontsize 6 octave))
			       (ly:clef::print grob))))

    \override Clef #'after-line-breaking = #(lambda (grob)
      (set! (ly:grob-property grob 'Y-offset) -1.25))

    %%% KEY %%%
    
    \remove "Key_engraver"

    %%% TIME SIGNATURE %%%
    
    % TO DO: "Counts Per Measure"
    \override TimeSignature #'before-line-breaking = #(lambda (grob)
      (set! (ly:grob-property grob 'Y-offset) 0.35))
    \numericTimeSignature

    %%% NOTES %%%
    
    \override NoteHead #'before-line-breaking = \NumberedNotesNoteHeads
    \override Stem #'no-stem-extend = ##t
    \remove "Accidental_engraver"

    %%% RESTS %%%
    
    \override Rest #'stencil = #(lambda (grob)
	  (let ((duration (ly:grob-property grob 'duration-log)))
	    (if (<= duration 2)
	      ; new rest symbol with exponent to denote duration
          (ly:stencil-translate 
	        (grob-interpret-markup grob
		      (markup #:concat
		        (#:char #x2205 
			     #:super (cond ((= -3 duration) "32") ; maxima
						       ((= -2 duration) "16") ; longa
						       ((= -1 duration) "8") ; breve
						       ((= 0 duration) "4") ; whole rest
						       ((= 1 duration) "2") ; half rest
						       ((= 2 duration) ""))))) ; quarter rest
	        ; reposition rests that need repositioning
	        (cond ((= 0 duration) '(0 . -1.5))
	              ((= 2 duration) '(0 . -0.5))
	              (else '(0 . 0))))
		  (ly:rest::print grob)) ; TO DO: stemmed rests
      ))
  }

  \context {
    \Staff
    \name StaffTester
    \alias Staff
    staffLineLayoutFunction = #ly:pitch-semitones
    \override StaffSymbol #'line-positions = #'(-9 -7 -5  -3   3 5 7 9 )
    % \override StaffSymbol #'ledger-positions = #'()
    \override Stem #'no-stem-extend = ##t
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \override TimeSignature #'before-line-breaking = #(lambda (grob) (set! (ly:grob-property grob 'Y-offset) 1.5))
    \numericTimeSignature
    % clefPosition = #3
    % middleCPosition = #-11
  }
  
  %%%%%%%%%%%%%%%%%%%%
  % setting where custom staff types can occur
  %
  \context { 
    \Score 
    \accepts StaffFiveLineOne
    \accepts StaffFiveLineTwo
    \accepts StaffParncuttOne
    \accepts StaffParncuttTwo
    \accepts StaffIsomorphOne
    \accepts StaffIsomorphTwo
    \accepts StaffKlavarMirckOne
    \accepts StaffKlavarMirckTwo
    \accepts StaffBeyreutherChromaticSixSix
    \accepts StaffBeyreutherUntitledOne
    \accepts StaffBeyreutherUntitledTwo
    \accepts StaffChromaticLyre
    \accepts StaffMutoOne
    \accepts StaffMutoTwo
    \accepts StaffTwinlineReed
    \accepts StaffTwinlineBlackTriangle
    \accepts StaffTwinlineBlackOval
    \accepts StaffTwinNote 
    \accepts StaffNumberedNotes
    \accepts StaffTester
  }
  \context { 
    \ChoirStaff
    \accepts StaffFiveLineOne
    \accepts StaffFiveLineTwo
    \accepts StaffParncuttOne
    \accepts StaffParncuttTwo
    \accepts StaffIsomorphOne
    \accepts StaffIsomorphTwo
    \accepts StaffKlavarMirckOne
    \accepts StaffKlavarMirckTwo
    \accepts StaffBeyreutherChromaticSixSix
    \accepts StaffBeyreutherUntitledOne
    \accepts StaffBeyreutherUntitledTwo
    \accepts StaffChromaticLyre
    \accepts StaffMutoOne
    \accepts StaffMutoTwo
    \accepts StaffTwinlineReed
    \accepts StaffTwinlineBlackTriangle
    \accepts StaffTwinlineBlackOval
    \accepts StaffTwinNote 
    \accepts StaffNumberedNotes
    \accepts StaffTester
  }
  \context { 
    \GrandStaff 
    \accepts StaffFiveLineOne
    \accepts StaffFiveLineTwo
    \accepts StaffParncuttOne
    \accepts StaffParncuttTwo
    \accepts StaffIsomorphOne
    \accepts StaffIsomorphTwo
    \accepts StaffKlavarMirckOne
    \accepts StaffKlavarMirckTwo
    \accepts StaffBeyreutherChromaticSixSix
    \accepts StaffBeyreutherUntitledOne
    \accepts StaffBeyreutherUntitledTwo
    \accepts StaffChromaticLyre
    \accepts StaffMutoOne
    \accepts StaffMutoTwo
    \accepts StaffTwinlineReed
    \accepts StaffTwinlineBlackTriangle
    \accepts StaffTwinlineBlackOval
    \accepts StaffTwinNote 
    \accepts StaffNumberedNotes
    \accepts StaffTester
  }
  \context { 
    \PianoStaff 
    \accepts StaffFiveLineOne
    \accepts StaffFiveLineTwo
    \accepts StaffParncuttOne
    \accepts StaffParncuttTwo
    \accepts StaffIsomorphOne
    \accepts StaffIsomorphTwo
    \accepts StaffKlavarMirckOne
    \accepts StaffKlavarMirckTwo
    \accepts StaffBeyreutherChromaticSixSix
    \accepts StaffBeyreutherUntitledOne
    \accepts StaffBeyreutherUntitledTwo
    \accepts StaffChromaticLyre
    \accepts StaffMutoOne
    \accepts StaffMutoTwo
    \accepts StaffTwinlineReed
    \accepts StaffTwinlineBlackTriangle
    \accepts StaffTwinlineBlackOval
    \accepts StaffTwinNote 
    \accepts StaffNumberedNotes
    \accepts StaffTester
  }
  \context {
    \StaffGroup 
    \accepts StaffFiveLineOne
    \accepts StaffFiveLineTwo
    \accepts StaffParncuttOne
    \accepts StaffParncuttTwo
    \accepts StaffIsomorphOne
    \accepts StaffIsomorphTwo
    \accepts StaffKlavarMirckOne
    \accepts StaffKlavarMirckTwo
    \accepts StaffBeyreutherChromaticSixSix
    \accepts StaffBeyreutherUntitledOne
    \accepts StaffBeyreutherUntitledTwo
    \accepts StaffChromaticLyre
    \accepts StaffMutoOne
    \accepts StaffMutoTwo
    \accepts StaffTwinlineReed
    \accepts StaffTwinlineBlackTriangle
    \accepts StaffTwinlineBlackOval
    \accepts StaffTwinNote 
    \accepts StaffNumberedNotes
    \accepts StaffTester
  }  
}

%%%%%%%%%%%%%%%%
% MUSIC FOR TESTING
%

%{

  theMusic = \relative c' { 
  % \clef treble
  \time 4/4
  c cis d dis e f fis g gis a ais b c d e f g a b c 
  \time 2/4
  \clef treble 
  <c,, e g>4
  \clef bass
  % \set Staff.middleCPosition = #18
  % \set StaffTwinlineReed.middleCPosition = #6
  % \set Staff.middleCOffset = #24
  <c e g>4 % f g a b2
  \clef treble 
  % \set Staff.middleCPosition = #-6
  % \set Staff.middleCOffset = #0
  <c e g>4
  % \clef alto
  % \set Staff.middleCPosition = #6
  % <c e g>4
}


  \new Staff { \numericTimeSignature \theMusic }
  \new StaffFiveLineOne { \theMusic }
  \new StaffFiveLineTwo { \theMusic }
  \new StaffParncuttOne { \theMusic }
  \new StaffParncuttTwo { \theMusic }
  \new StaffIsomorphOne { \theMusic }
  \new StaffIsomorphTwo { \theMusic }
  \new StaffBeyreutherChromaticSixSix { \theMusic }
  \new StaffBeyreutherUntitledOne { \theMusic }
  \new StaffBeyreutherUntitledTwo { \theMusic }
  \new StaffChromaticLyre { \theMusic }
  \new StaffTwinlineReed { \theMusic }
  \new StaffTwinlineBlackTriangle { \theMusic }
  \new StaffTwinlineBlackOval { \theMusic }
  \new StaffTwinNote { \theMusic }
  \new StaffMutoOne { \theMusic }
  \new StaffMutoTwo { \theMusic }
  \new StaffKlavarMirckOne { \theMusic }
  \new StaffKlavarMirckTwo { \theMusic }
  \new StaffNumberedNotes { \theMusic }
  % \new StaffTester { \theMusic }
  
%}