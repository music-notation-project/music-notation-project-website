\version "2.12.0"
\header{
  texidoc="@code{staffLineLayoutFunction} is used to change the position of the notes.
This sets @code{staffLineLayoutFunction} to @code{ly:pitch-semitones} to
produce a chromatic scale with the distance between a consecutive
space and line equal to one semitone.
"
}

%{ 
Comment: This file demonstrates the use of the following to achieve various chromatic staff notation systems:

     staffLayoutFunction = #ly:pitch-semitones 
          (customizes the note placement on the staff to chromatic staff layout, semitone spacing)

     staffLineLayoutFunction = #(lambda (p) (floor (/ (ly:pitch-semitones p) 2)))
          (customizes the note placement on the staff to wholetone spacing, 2 notes at each staff position.  
           The number 2 corresponds to the number of notes placed at a given staff position,
           so it can be customized to other values.)
     
     middleCPosition = #-6
          (changes the vertical position of middle C and all other notes in turn)

     clefPosition    
          (changes the vertical position of the clef)

     \override Staff.StaffSymbol #'line-positions = #'( 4 2 0 -2 -4 )    
          (customizes the location of staff lines.  4 2 0 -2 -4 are the positions for the traditional staff)

     \remove "Accidental_engraver"   
          (Prevents accidental signs from being drawn)

     \remove "Key_engraver"          
          (Prevents key signature from being drawn)

     \override NoteHead #'style = #style-notehead
		  (Assigns custom notehead styles)
%}

%Begin notehead customization functions

%Association list of pitches to note head styles.
%Grouped by pitch. Defaults: 0 is C, 1 is D, ... 6 is B.
% note head styles: "do re mi" (shape note) or "cross, mensural, default" etc.
#(define style-mapping
   (list
    (cons (ly:make-pitch 0 6 SHARP) 'do)
    (cons (ly:make-pitch 0 0 NATURAL) 'do)
    (cons (ly:make-pitch 0 1 DOUBLE-FLAT) 'do)

    (cons (ly:make-pitch 0 6 DOUBLE-SHARP) 'cross)
    (cons (ly:make-pitch 0 0 SHARP) 'cross)
    (cons (ly:make-pitch 0 1 FLAT) 'cross)

    (cons (ly:make-pitch 0 0 DOUBLE-SHARP) 're)
    (cons (ly:make-pitch 0 1 NATURAL) 're)
    (cons (ly:make-pitch 0 2 DOUBLE-FLAT) 're)

    (cons (ly:make-pitch 0 1 SHARP) 'diamond)
    (cons (ly:make-pitch 0 2 FLAT) 'diamond)
    (cons (ly:make-pitch 0 3 DOUBLE-FLAT) 'diamond)

    (cons (ly:make-pitch 0 1 DOUBLE-SHARP) 'mi)
    (cons (ly:make-pitch 0 2 NATURAL) 'mi)
    (cons (ly:make-pitch 0 3 FLAT) 'mi)

    (cons (ly:make-pitch 0 2 SHARP) 'fa)
    (cons (ly:make-pitch 0 3 NATURAL) 'fa)
    (cons (ly:make-pitch 0 4 DOUBLE-FLAT) 'fa)

    (cons (ly:make-pitch 0 2 DOUBLE-SHARP) 'default)
    (cons (ly:make-pitch 0 3 SHARP) 'default)    
	(cons (ly:make-pitch 0 4 FLAT) 'default)

    (cons (ly:make-pitch 0 3 DOUBLE-SHARP) 'triangle)
    (cons (ly:make-pitch 0 4 NATURAL) 'triangle)
    (cons (ly:make-pitch 0 5 DOUBLE-FLAT) 'triangle)

    (cons (ly:make-pitch 0 4 SHARP) 'harmonic)
    (cons (ly:make-pitch 0 5 FLAT) 'harmonic)

    (cons (ly:make-pitch 0 4 DOUBLE-SHARP) 'la)
    (cons (ly:make-pitch 0 5 NATURAL) 'la)
    (cons (ly:make-pitch 0 6 DOUBLE-FLAT) 'la)

    (cons (ly:make-pitch 0 5 SHARP) 'mensural)
    (cons (ly:make-pitch 0 6 FLAT) 'mensural)
    (cons (ly:make-pitch 0 0 DOUBLE-FLAT) 'mensural)

    (cons (ly:make-pitch 0 5 DOUBLE-SHARP) 'ti)
    (cons (ly:make-pitch 0 6 NATURAL) 'ti)
    (cons (ly:make-pitch 0 0 FLAT) 'ti)))

%Compare pitch and alteration (not octave).
#(define (pitch-equals? p1 p2)
   (and
    (= (ly:pitch-alteration p1) (ly:pitch-alteration p2))
    (= (ly:pitch-notename p1) (ly:pitch-notename p2))))

#(define (pitch-to-style pitch)
   (let ((style (assoc pitch style-mapping pitch-equals?)))
     (if style
         (cdr style))))

#(define (style-notehead grob)
   (pitch-to-style
    (ly:event-property (event-cause grob) 'pitch)))

%End notehead customization functions



scales = \relative c' {
  c cis d dis e f fis g gis a ais b bisis c c b bes a aes g ges f e ees d des c
}



\new Staff \with {
  \remove "Accidental_engraver"
  \remove "Key_engraver" 
  staffLineLayoutFunction = #ly:pitch-semitones
  middleCPosition = #-6
  clefGlyph = #"clefs.G"
  clefPosition = #(+ -6 7)
}
{
  \override Staff.StaffSymbol #'line-positions = #'( 4 2 0 -2 -4 )
  \override NoteHead #'style = #style-notehead
  <<
    \scales
    \context NoteNames {
      \set printOctaveNames= ##f
      \scales
    }
  >>
}

\new Staff \with {
  \remove "Accidental_engraver"
  \remove "Key_engraver" 
  staffLineLayoutFunction = #(lambda (p) (floor (/ (ly:pitch-semitones p) 2)))
  middleCPosition = #-6
  clefGlyph = #"clefs.G"
  clefPosition = #(+ -6 7)
}
{
  \override Staff.StaffSymbol #'line-positions = #'( 4 2 -2 -4 )
  \override NoteHead #'style = #style-notehead
  <<
    \scales
    \context NoteNames {
      \set printOctaveNames= ##f
      \scales
    }
  >>
}
