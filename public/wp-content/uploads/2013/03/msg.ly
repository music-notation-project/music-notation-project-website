% MSG (Modified Schoenberg Notation for Guitar) by Ole Kirkeby, Paul Morris, and Jan Braunstein
% SLING (Schoenberg-Lyrebird Isomorphic Notation for Guitar) by Ole Kirkeby and Jan Braunstein

#(if (not (defined? 'LN-key-signature)) (define LN-key-signature #f))
% variable for displaying key signature reminder (options are ##t or ##f)

#(if (not (defined? 'LN-chords)) (define LN-chords "B"))
% LN-chords is a LyreBird variable for tweaking the way the chords are displayed. The options are  ##t, ##f, H or B. LyreBird sets LN-chords to ##t by default and uses German nomencalture (H for B). The line above sets the default to English B instead.


\version "2.18.2"
\include "lyrebird.ly"

staff-sling = {
  \stopStaff \startStaff
  \override Staff.StaffSymbol.line-positions = #'( -8 0 4 12 )
  \resetStaffY-offset

  \override Score.KeySignature.whiteout = ##t %just an idea for better readability of the key-reminder if used

  \setRestPos-2  % neat function in LyreBird - sets the staff position for rests. However, this might cause problems in polyphonic music. In such case \restPos & \resetRestPos commands can be usefull. Without using \setRestPos function or after \resetRestPos is triggerd (which reverts \setRestPos to default value) the rests in polyphony are placed automaticaly to avoid other objects.
}

%-------------------------------------------------------------------------------
% Experimental staff (LN staff-IV with G#/Ab line dashed)
%-------------------------------------------------------------------------------


%%http://lsr.di.unimi.it/LSR/Item?id=880

% original code (for zig-zag lines) by Thomas Morley (Harm)
% -> http://lists.gnu.org/archive/html/lilypond-user/2012-12/msg00715.html
% slightly modified to create dashed lines by Paul Morris

dashedStaffSymbolLines =
#(define-music-function (parser location dash-space bool-list)
   ((number-pair? '(0.5 . 0.5)) list?)
   "
Replaces specified lines of a StaffSymbol with dashed lines.

The lines to be changed should be given as a list containing booleans, with
the meaning:
  #f - no dashes, print a normal line
  #t - print a dashed line
The order of the bool-list corresponds with the order of the given list of
'line-positions or if not specified, with the default.
If the length of the bool-list and the 'line-positions doesn't match a warning
is printed.

The width of the dashes and the spacing between them can be altered by adding a pair
as first argument while calling the function:
\\dashedStaffSymbolLines #'(1 . 1) #'(#f #f #t #f #f)
the first number of the pair is the width, the second the spacing
"
   #{
     \override Staff.StaffSymbol.after-line-breaking =
     #(lambda (grob)
        (let* ((staff-stencil (ly:grob-property grob 'stencil))
               (staff-line-positions
                (if (equal? (ly:grob-property grob 'line-positions) '() )
                    '(-4 -2 0 2 4)
                    (ly:grob-property grob 'line-positions)))
               (staff-width
                (interval-length
                 (ly:stencil-extent staff-stencil X)))
               (staff-space (ly:staff-symbol-staff-space grob))
               (staff-line-thickness (ly:staff-symbol-line-thickness grob))
               ;; width of the dash
               (dash-width (car dash-space))
               ;; space between dashes
               (space-width (cdr dash-space))
               ;; Construct the first dash
               (sample-path `((moveto 0 0)
                              (lineto ,dash-width 0)
                              ))
               ;; Make a stencil of the first dash
               (dash-stencil
                (grob-interpret-markup
                 grob
                 (markup
                  #:path staff-line-thickness sample-path)))
               ;; width of both dash and space
               (dash-space-width (+ dash-width space-width))

               ;; another way: get width of dash from the dash stencil
               ;; (stil-width
               ;;   (interval-length
               ;;     (ly:stencil-extent dash-stencil X)))
               ;; (dash-space-width (+ stil-width space-width))

               ;; Make a guess how many dashes are needed.
               (count-dashes
                (inexact->exact
                 (round
                  (/ staff-width
                    (- dash-space-width
                      staff-line-thickness)))))
               ;; Construct a stencil of dashes with the guessed count
               (dashed-stil
                (ly:stencil-aligned-to
                 (apply ly:stencil-add
                   (map
                    (lambda (x)
                      (ly:stencil-translate-axis
                       dash-stencil
                       (* (- dash-space-width staff-line-thickness) x)
                       X))
                    (iota count-dashes)))
                 Y
                 CENTER))
               ;; Get the the length of that dashed stencil
               (stil-x-length
                (interval-length
                 (ly:stencil-extent dashed-stil  X)))
               ;; Construct a line-stencil to replace the staff-lines.
               (line-stil
                (make-line-stencil staff-line-thickness 0 0 staff-width 0))
               ;; Calculate the factor to scale the dashed-stil to fit
               ;; the width of the original staff-symbol-stencil
               (corr-factor
                (/ staff-width (- stil-x-length staff-line-thickness)))
               ;; Construct the new staff-symbol
               (new-stil
                (apply
                 ly:stencil-add
                 (map
                  (lambda (x y)
                    (ly:stencil-translate
                     (if (eq? y #f)
                         line-stil
                         (ly:stencil-scale
                          dashed-stil
                          corr-factor 1))
                     (cons (/ staff-line-thickness 2)
                       (* (/ x 2) staff-space))))
                  staff-line-positions bool-list))))

          (if (= (length bool-list)(length staff-line-positions))
              (ly:grob-set-property! grob 'stencil new-stil)
              (ly:warning
               "length of dashed line bool-list doesn't match the line-positions - ignoring"))))
   #})

%-------------------------------------------------------------------------------


staff-msg = {
  \stopStaff \startStaff
  \override Staff.StaffSymbol.line-positions = #'(-8 -4 0 4 8 12 16 20 24 28)
  \dashedStaffSymbolLines #'(#f #t #t #f #t #t #f #t #t #f)
  \resetStaffY-offset
  \setRestPos -1
}


%-------------------------------------------------------------------------------
% Repeat Dots
%-------------------------------------------------------------------------------

#(if (not TN)
     (define ((make-custom-dot-bar-line dot-positions) grob extent)
       (let* ((staff-space (ly:staff-symbol-staff-space grob))
              (dot (ly:font-get-glyph (ly:grob-default-font grob) "dots.dot"))
              (stencil empty-stencil))
         (for-each
          (lambda (dp)
            (set! stencil (ly:stencil-add stencil
                            (ly:stencil-translate-axis dot (* dp (/ staff-space 2)) Y))))
          dot-positions) stencil
         (ly:stencil-translate-axis stencil -1.75 Y))))


%-------------------------------------------------------------------------------


%-------------------------------------------------------------------------------
% TN <-> MSG conversion
%-------------------------------------------------------------------------------

#(if TN (begin
         (set! staff-sling tn-staff-I)
         (set! staff-msg tn-staff-I)
         ))


