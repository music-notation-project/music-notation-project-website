---
import MnpLayoutBlog from "../../../../../layouts/MnpLayoutBlog.astro";
import { makeMetaTitle } from "../../../../../utils/utils";
---

<MnpLayoutBlog
  metaTitle={makeMetaTitle('Review of E.M. Hume\'s "Supermusicology"', "Blog")}
  metaDescription="A blog post reviewing a book by E.M. Hume called 'Supermusicology'"
>
  <div id="primary">
    <div id="content" role="main">
      <nav id="nav-single">
        <h3 class="assistive-text">Post navigation</h3>
        <span class="nav-previous"
          ><a href="/blog/2008/09/finale-notepad-2006-get-it/" rel="prev"
            ><span class="meta-nav">&larr;</span> Previous Post</a
          >
        </span>
        <span class="nav-next"
          ><a href="/blog/2009/02/enharmonic-equivalents-tutorial/" rel="next"
            >Next Post <span class="meta-nav">&rarr;</span>
          </a>
        </span>
      </nav>

      <article
        class="post type-post status-publish format-standard hentry category-uncategorized"
      >
        <header class="entry-header">
          <h1 class="entry-title">Review of E.M. Hume's "Supermusicology";</h1>

          <div class="entry-meta">
            <span class="sep">Posted on</span>
            <time class="entry-date" datetime="2009-02-04T20:00:00+00:00"
              >February 4, 2009</time
            ><span class="by-author">
              <span class="sep"> by</span>
              <span class="author vcard"
                ><a
                  class="url fn n"
                  href="/blog/author/mnpadmin/"
                  title="View all posts by Webmaster"
                  rel="author">Webmaster</a
                >
              </span>
            </span>
          </div>
        </header>

        <div class="entry-content">
          <p>by Paul Morris</p>
          <p>
            <span style="font-style: italic;">Supermusicology</span> is Ernest Moore
            Hume&#8217;s book about his alternative music notation system “SuperMusic”
            and how it seeks to improve upon traditional music notation. It is written
            in a conversational tone with many helpful illustrations. I particularly
            enjoyed reading the history section of the book and its account of the
            use of various staves with different numbers of lines in western music
            history.<span id="more-488"></span>
          </p>
          <p>
            The thoroughness and effort that Hume has put into his book comes
            through in his attention to certain details. For example, a brief
            and intriguing passage that is of general interest refers to the
            Human Engineering Laboratory and their work on isolating and
            identifying particular human aptitudes. This now-defunct
            organization (formerly based in Boston, Massachusetts, USA)
            identified three basic aptitudes relevant to musicianship: tonal
            memory, rhythm memory, and pitch discrimination. Hume notes how
            these are different skills from those needed to read music, since
            reading music, the “ability to discern changes in written material”
            is an unrelated “accounting aptitude” (page 27). One could have
            excellent aptitude(s) for playing music, but still struggle to read
            music notation since that is a separate and unrelated skill.
          </p>
          <p>
            <span style="font-style: italic;">Supermusicology</span> gives Hume&#8217;s
            account of the problems with traditional notation. These are largely
            the same issues identified by the Music Notation Project (see our <a
              href="/">Intro</a
            >), although he does not emphasize the inconsistent appearance of
            intervals in traditional notation. To these issues he adds the
            difficulty of reading notes with too many ledger lines, a judgment
            with which I and I assume most supporters of the Music Notation
            Project would concur. Hume goes on to describe his SuperMusic
            notation system and how it addresses these issues.
          </p>
          <p>
            Hume notes that SuperMusic introduces two major modifications to the
            way music is written: A) a seven-line chromatic staff, and B) “pitch
            bars” that indicate the octave in which a note is to be played. It
            also introduces a few “minor” modifications like re-naming the 12
            notes of the chromatic scale by the numerals 1-12 (with C being 1).
            The rest of the elements of traditional notation are retained,
            including its rhythmic notation system. He notes the value of this
            consistency, taking the approach of “if it&#8217;s not broken,
            don&#8217;t try to fix it.”
          </p>
          <p>
            The seven lines of SuperMusic&#8217;s chromatic staff are spaced a
            whole step apart with the notes of the chromatic scale falling
            either on the lines or the spaces between them. The top, bottom, and
            center lines are bold, with two normal lines falling between each
            bold line. The top and bottom lines represent C and the middle line
            F#/Gb.
          </p>
          <p>
            In his justification for this seven-line staff, Hume makes an
            interesting point about the difficulty of visualizing more than
            three staff lines at a time.
          </p>
          <p>
            “Take a second and try to visualize six parallel lines in your
            mind’s eye. For most people this is virtually impossible. However,
            it is only slightly easier to visualize five parallel lines. Oddly,
            in attempting to do this you may notice that you can handle it if
            you think of the lines in sections, that is two sets of three lines
            in the case of a six-line staff, or three and two, in the case of a
            five-line staff. In fact, almost anyone can visualize three parallel
            lines, an important point in the development of the new staff.”
            (page 43)
          </p>
          <p>
            This seems to provide a general argument for the use of a staff with
            fewer lines, and preferably three or less. However, Hume uses it as
            part of his rationale for the SuperMusic staff with its three bold
            lines, which can then be mentally subdivided into two contiguous
            three line staves (with the third bold line appearing above them).
          </p>
          <p>
            Hume describes how the SuperMusic staff is based on the basic 6-line
            diatonic staff that was used by Jan Sweelinck (1562-1621) and
            Girolamo Frescobaldi (1583-1643). He has added the seventh line in
            the interest of the aesthetic symmetry of the three bold lines and
            to keep any of the twelve basic chromatic notes from falling in the
            spaces above or below the span of the staff.
          </p>
          <p>
            What most sets SuperMusic apart from other chromatic-staff based
            notation systems is its use of “pitch bars.” These are short
            vertical hash marks that occur before or after each note to indicate
            the octave in which the note is to be played. They function somewhat
            like the 8va symbol in traditional notation, raising or lowering a
            note by one or more octaves:
          </p>
          <p>
            3 pitch bars before a note means it is played 3 octaves lower<br />
            2 pitch bars before a note means it is played 2 octaves lower<br />
            1 pitch bar before a note means it is played 1 octave lower
          </p>
          <p>
            0 pitch bars means it is played in the octave of the current staff*
          </p>
          <p>
            1 pitch bars after a note means it is played 1 octave higher<br />
            2 pitch bars after a note means it is played 2 octaves higher<br />
            3 pitch bar after a note means it is played 3 octaves higher
          </p>
          <p>
            (* presumably as indicated by a register indication at the beginning
            of the staff.)
          </p>
          <p>
            This provides a seven octave span covering the pitch range of the
            piano keyboard on a single seven-line staff.
          </p>
          <p>
            The use of pitch bars is motivated by Hume&#8217;s view that one of
            the main problems with traditional notation is its use of ledger
            lines &#8212; that it becomes too difficult to tell what pitch a
            note is when it falls above or below the staff and requires multiple
            ledger lines. No ledger lines are used in SuperMusic, since pitch
            bars make them unnecessary. When a melody ascends beyond the top of
            the staff the notes begin to appear an octave lower with a pitch bar
            written before each of them to indicate that they are to be played
            an octave higher.
          </p>
          <p>
            This brings me to a potential criticism of Hume&#8217;s system that
            is worth mentioning. Hume is critical of traditional
            notation&#8217;s use of 8va and accidental signs, but it would seem
            that SuperMusic&#8217;s pitch bars require the same kind of two-step
            process in order to determine the pitch of a note. A consistent
            vertical pitch axis is compromised as notes falling above or below
            others on the staff may be lower or higher in pitch depending on the
            presence or absence of pitch bars. The appearance of intervals would
            also be less consistent. Intervals that extended off the staff would
            appear just like their inversions until one took into account the
            notes&#8217; pitch bars. Each interval would have two basic
            appearances depending on whether they “wrap” around the top or
            bottom of the staff or not. For instance, a major third and a minor
            sixth would each have two visual configurations that would be
            indistinguishable except for their pitch bars.
          </p>
          <p>
            Hume is right to raise the difficulty of identifying notes that
            require many ledger lines. This is a problem with traditional
            notation that deserves to be addressed. However, it seems that one
            could solve it by making a visual distinction between ledger lines.
            For instance, one could make ledger lines wider or narrower to
            indicate which ones represented lines that fall between staves, and
            which represented lines that would be part of an additional staff
            should one be drawn above (or below) the current staff.
          </p>
          <p>
            Another potential point of criticism worth briefly mentioning is
            whether SuperMusic&#8217;s seven line staff is actually superior to
            a similar staff with five or six lines. Hume does consider a five or
            six line staff but concludes that seven lines are preferable,
            although it seems that this remains debatable.
          </p>
          <p>
            With regard to the Music Notation Project&#8217;s desirable <a
              href="/systems/criteria/"
              >criteria for alternative music notation systems</a
            >, SuperMusic does not meet either the <a
              href="/systems/criteria/#8">eighth</a
            > or <a href="/systems/criteria/#15">fifteenth</a> criteria.
          </p>
          <p>
            Of course, most any notation system will involve tradeoffs in its
            design that reflect the goals and priorities of its designer. Hume
            has put a lot of effort into studying the history and disadvantages
            of traditional notation and designing SuperMusic to address them.
            This comes through in his thorough presentation of SuperMusic and
            his reasoning behind it. Supermusicology does well to raise the
            issue of the difficulty of reading notes that require many ledger
            lines and offers some helpful insights into the business of
            attempting to improve upon traditional music notation. While there
            may be differences in the way notation designers address the
            disadvantages of traditional music notation, it is encouraging to
            see books like Supermusicology and the growing consensus on the
            significance of these disadvantages and the confidence that they can
            be addressed by a better approach.
          </p>
          <p>
            – Supermusicology is available from <a
              href="http://www.lulu.com/content/2034904">Lulu.com</a
            >
          </p>
        </div>
      </article>
    </div>
  </div>
</MnpLayoutBlog>
