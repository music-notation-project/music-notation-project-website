---
import MnpLayout from "../../layouts/MnpLayout.astro";
import { makeMetaTitle } from "../../utils/utils";
---

<MnpLayout
  metaTitle={makeMetaTitle("Software")}
  metaDescription="Software for alternative music notation systems to better compare, evaluate, and use them. Learn about the Music Notation Project's efforts in this area."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="page type-page status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Software</h1>
        </header>

        <div class="entry-content">
          <p>
            Alternative music notation systems are appealing in theory, but what
            about the practical need for sheet music?<a
              href="/software/#ftn1"
              id="ftnref1">[1]</a
            > Fortunately, there are now software applications that can display and
            print music in a variety of chromatic-staff notation systems, easing
            the burden of transcribing (or &#8220;transnotating&#8221;) sheet music.
          </p>
          <h2 class="divider-heading">
            Applications That Support Chromatic Staff Notation Systems
          </h2>
          <table>
            <tbody>
              <tr>
                <td
                  ><a href="/software/lilypond/"
                    ><img
                      loading="lazy"
                      class="wp-image-1223 alignnone"
                      src="/wp-content/uploads/2013/03/double-lily150x150.gif"
                      alt="LilyPond Logo"
                      width="95"
                      height="95"
                    />
                  </a>
                </td>
                <h3>
                  <a href="/software/lilypond/">LilyPond</a>
                </h3>
                <p>
                  LilyPond is a free, open-source, cross-platform application
                  that is highly customizable and can be used to create sheet
                  music for chromatic-staff notation systems. <a
                    href="/software/lilypond/">More info&#8230;</a
                  >
                </p>
                <td></td>
                <td
                  ><a href="/software/finale/"
                    ><img
                      loading="lazy"
                      class="alignnone size-full wp-image-1224"
                      src="/wp-content/uploads/2013/03/Finale2010logo.gif"
                      alt="Finale Logo"
                      width="75"
                      height="75"
                    />
                  </a>
                </td>
                <h3>
                  <a href="/software/finale/">Finale</a>
                </h3>
                <p>
                  Finale is a commercial software package that has features that
                  can be used to edit and create sheet music for chromatic-staff
                  notation systems. <a href="/software/finale/">
                    More info&#8230;</a
                  >
                </p>
              </tr>
            </tbody>
          </table>
          <p>
            Both of these applications allow you to customize the position of
            the notes on the staff, and make other relevant customizations.
            Using either of them you can:
          </p>
          <ul>
            <li>
              Create musical scores in an alternative notation system
              &#8220;from scratch&#8221; by inputting the notes using your
              mouse, keyboard, or MIDI device.
            </li>
            <li>
              Use music scanning software to scan and convert sheet music into a
              digital file format and then transnotate it into an alternative
              notation system.
            </li>
            <li>
              Automatically convert existing digital music notation files into
              an alternative notation system. Converting files from one type to
              another is now much easier thanks to <a
                href="http://www.musicxml.com">MusicXML,</a
              > an interchange file format for music notation. There is a growing
              number of sheet music files available for download from online libraries
              like these:
              <ul>
                <li>
                  <a
                    href="http://www.mutopiaproject.org/"
                    target="_blank"
                    rel="noopener">Mutopia Project</a
                  >
                </li>
                <li>
                  <a
                    href="http://www.gutenberg.org/wiki/Gutenberg:The_Sheet_Music_Project"
                    target="_blank"
                    rel="noopener">Gutenberg Sheet Music Project</a
                  >
                </li>
                <li>
                  <a
                    href="http://www.wikifonia.org/"
                    target="_blank"
                    rel="noopener">Wikifonia</a
                  >
                </li>
                <li>
                  <a href="http://imslp.org" target="_blank" rel="noopener"
                    >International Music Score Library Project (IMSLP)</a
                  >
                </li>
              </ul>
            </li>
          </ul>
          <p>
            One of our long-term goals has been to adapt or extend existing
            software to display and print music in a variety of alternative
            notation systems. (For an in-depth discussion see <a
              href="/software/open-source-strategy/">Open-Source Strategy</a
            >.) Thanks to the efforts of a number of people this has now become
            a reality. It is now much easier to use, evaluate, and compare
            different alternative music notation systems, and the &#8220;lack of
            sheet music&#8221; problem is not nearly as daunting as it was just
            a few years ago. However, there is still room for improvement, so if
            you might be interested in helping out with our efforts in this area
            please <a href="/home/about-faq-contact-info/#contact">contact us</a
            >.
          </p>
          <h2 class="divider-heading">
            Could Potentially Support Alternative Notation Systems
          </h2>
          <p>
            <a href="/software/musescore/"
              ><img
                loading="lazy"
                class="alignnone size-full wp-image-1225"
                src="/wp-content/uploads/2013/03/musescore-mu-whitebg-xs.png"
                alt="MuseScore Logo"
                width="100"
                height="85"
              />
            </a>
          </p>
          <h3>
            <a href="/software/musescore/">MuseScore</a>
          </h3>
          <p>
            MuseScore is a free, open-source, cross-platform music notation
            editing application with a graphical user interface. It offers
            import and export to and from MusicXML and it supports plug-ins. Jan
            Braunstein has used MuseScore to create sheet music for his <a
              href="/system/chromatic-lyre-notation-by-jan-braunstein/"
              >Chromatic Lyre Notation</a
            >, but the process is not automated and is more of a workaround than
            a full solution (since MuseScore does not offer the ability to
            customize the positions of notes on the staff). <a
              href="/software/musescore/">More info&#8230;</a
            >
          </p>
          <h3>
            <a href="/software/sibelius/">Sibelius</a>
          </h3>
          <p>
            We briefly looked into the possibility of creating a plug-in for
            Sibelius that would provide support for chromatic staff notation
            systems. <a href="/software/sibelius/">More info&#8230;</a>
          </p>
          <h3>Canorus</h3>
          <p>
            <a
              href="http://sourceforge.net/apps/mediawiki/canorus/index.php?title=Main_Page"
              target="_blank"
              rel="noopener">Canorus</a
            > is a free, open-source, cross-platform music notation editing application
            with a graphical user interface. It offers import from and export to
            the LilyPond file format.
          </p>
          <h3>Notation Composer</h3>
          <p>
            <a href="http://notation.com/" target="_blank" rel="noopener"
              >Notation Composer</a
            > is a commercial application for Windows. Its author has communicated
            to us that he would be willing to consider working with a programmer
            who was interested in adding plug-in support for chromatic staves to
            it.
          </p>
          <h2 class="divider-heading">
            Interesting Software Libraries and APIs
          </h2>
          <p>
            <em
              >Note that these are not stand-alone notation editor applications
              like the others on this page, but are resources that might
              interest programmers.</em
            >
          </p>
          <h3>VexFlow</h3>
          <p>
            <a href="http://vexflow.com/" target="_blank" rel="noopener"
              >VexFlow</a
            > is an open-source, web-based, JavaScript library for rendering traditional
            music notation and guitar tablature. It uses HTML5 Canvas and SVG graphics,
            and can display music notation in a web browser (ie: on a web page or
            in a web application).
          </p>
          <h3>Belle, Bonne, Sage</h3>
          <p>
            <a
              href="https://github.com/burnson/Belle"
              target="_blank"
              rel="noopener">Belle, Bonne, Sage</a
            > is a free, open-source, C++ vector-graphics library for music notation.
            It makes no assumptions about the graphical layout of music notation.
            This gives it a built-in flexibility that makes it well-suited for alternative
            forms of music notation.<a id="apps"></a>
          </p>
          <h2 class="divider-heading">
            Applications other parties have created for alternative notation
            systems
          </h2>
          <p>
            Our efforts work towards permitting the transnotation of music into
            a wide variety of chromatic-staff notation systems. The following
            applications each work with a specific alternative notation system,
            and were developed independently from the Music Notation Project.
          </p>
          <h3>Ambrose Piano Tabs Music Editing Program</h3>
          <p>
            This program for <a
              href="http://www.ambrosepianotabs.com/"
              target="_blank"
              rel="noopener">Ambrose Piano Tabs</a
            > notation is free to download and use for noncommercial purposes. It
            can read MIDI files and supports editing and playback. It is closed-source
            and presumably runs only on Windows (no system requirements are listed
            on their website). See the <a
              href="http://www.ambrosepianotabs.com/Content.aspx?id=37"
              target="_blank"
              rel="noopener">lessons page</a
            > and <a
              href="http://www.ambrosepianotabs.com/Package.aspx"
              target="_blank"
              rel="noopener">installation package page</a
            > to download it.
          </p>
          <h3>KlavarScript</h3>
          <p>
            <a href="http://www.klavar.com/en/" target="_blank" rel="noopener"
              >KlavarScript</a
            > is an application for Klavar notation. It runs on Windows and is available
            as a free download. The December 2005 version imports both MIDI and MusicXML
            files.
          </p>
          <h3>Klavar Music Writer</h3>
          <p>
            Klavar Music Writer (originally called KlavarWriterXP) is a more
            recent application for Klavar notation. Its features include the
            ability to input music through a MIDI keyboard. It runs on Windows
            and is available as a free download from the <a
              href="http://www.klavarmusic.org/"
              >Klavar Music Foundation of Great Britain</a
            >.
          </p>
          <h3>KLAVAR!</h3>
          <p>
            <a
              href="http://sourceforge.net/projects/klavar/"
              target="_blank"
              rel="noopener">KLAVAR!</a
            > is an open source graphic music sequencer/notation program for Klavar
            notation that is now being ported to Windows, Mac, and Linux. Work on
            it seems to have begun in August 2009. It can be downloaded for free
            from <a
              href="http://sourceforge.net/projects/klavar/"
              target="_blank"
              rel="noopener">Sourceforge</a
            >. It was originally connected to the <a
              href="http://www.klavarscore.co.uk/"
              target="_blank"
              rel="noopener">KlavarScore</a
            > website, but this seems to have changed as the links to it have been
            removed.
          </p>
          <h3>Pizzicato</h3>
          <p>
            Pizzicato is music notation software from Arpege Music that <a
              href="http://www.arpegemusic.com/alternative-music-notation.htm"
              target="_blank"
              rel="noopener">supports numeric notation systems</a
            > that use twelve single characters per octave including <a
              href="https://en.wikipedia.org/wiki/Numbered_musical_notation"
              target="_blank"
              rel="noopener">Jianpu</a
            > and <a
              href="http://hamburgmusicnotation.com/"
              target="_blank"
              rel="noopener">Hamburg Music Notation</a
            >.  It is closed-source commercial software and runs on Windows or
            Mac OS X. It comes in a wide range of editions (choir, guitar,
            light, professional, etc.) ranging up to 299 euros in price.
          </p>
          <h3>NoteWriter-AB (no longer available)</h3>
          <p>
            Albert Brennink had Keith Hamel create &#8220;NoteWriter-AB,&#8221;
            an application for Brennink&#8217;s <a
              href="/system/a-b-chromatic-notation-by-albert-brennink/"
              >A-B Chromatic Notation</a
            >. It was an extended version of the <a
              href="http://debussy.music.ubc.ca/NoteWriter/NWwelcome.html"
              target="_blank"
              rel="noopener">NoteWriter II®</a
            > software for Apple Macintosh computers, and Brennink sold it for $295.
            At this point it is &#8220;legacy&#8221; software, as it only runs in
            Mac OS 9 or the Classic environment of OS X for PowerPC Macs, and will
            not run on Intel Macs at all.
          </p>
          <div class="footnotes divider-section">
            <a href="/software/#ftnref1" id="ftn1">[1]</a> Most alternative notation
            systems do not have large catalogues of sheet music available for them.
            The biggest exception is <a
              href="http://www.klavarmusic.org"
              target="_blank"
              rel="noopener">Klavar</a
            >. The <a
              href="http://www.klavarskribo.nl/"
              target="_blank"
              rel="noopener">Klavar-Foundation in the Netherlands</a
            > offers over 25,000 scores and music books, totalling an estimated 200,000
            musical works. The <a
              href="http://www.klavarmusic.org/"
              target="_blank"
              rel="noopener">Klavar Music Foundation of Great Britain</a
            > also has an extensive catalogue.  The Music Notation Project&#8217;s
            <a href="/wiki/">Wiki</a> provides some sheet music in various alternative
            notation systems and a list of <a
              href="/wiki/sheet-music/sheet-music-sources/"
              >Other Sheet Music Sources</a
            > for particular alternative notation systems.
          </div>
        </div>
      </article>
    </div>
  </div>
</MnpLayout>
