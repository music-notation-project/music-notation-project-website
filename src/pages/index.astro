---
import MnpLayout from "../layouts/MnpLayout.astro";
---

<MnpLayout
  metaTitle="The Music Notation Project — Alternative music notation systems"
  metaDescription="Explore alternative music notation systems that improve upon traditional music notation. Learn how a chromatic staff is better. Get resources & software."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="page type-page status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Improving Upon Traditional Music Notation</h1>
        </header>

        <div class="entry-content">
          <p>
            Many people struggle to learn to read and play music, and many give
            up before they become proficient. Could a better notation system
            make reading, writing, and playing music more enjoyable and easier
            to learn? We think so.
          </p>
          <h2 class="likeH3">The Chromatic Staff Approach</h2>
          <p>
            Here is a chromatic scale on a traditional diatonic staff (above)
            and the same chromatic scale on a chromatic staff with five lines
            (below). This is just one of many versions of chromatic staff.
          </p>
          <p>
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/diatonicTreble577.png"
              alt="Twelve note chromatic scale from C to C with five sharp signs, on a standard diatonic musical staff"
              width="577"
              height="100"
            />
          </p>
          <p>
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/chromatic5line577.png"
              alt="Twelve note chromatic scale from C to C on a chromatic musical staff with five lines"
              width="577"
              height="114"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            When you listen to the scale, which staff best represents what you
            hear?
          </p>
          <p>
            <audio preload="auto" controls="controls"
              ><source
                src="/audio/chromaticscale/chromaticscale6.oga"
                type="audio/ogg"
              /><source src="/audio/chromaticscale/chromaticscale6.mp3" />
            </audio>
          </p>
          <p>
            On a chromatic staff each note has its own line or space on the
            staff. On the traditional staff only seven notes have their own line
            or space, the notes from just one key (C major/A minor, the white
            keys on the piano). The remaining notes (the black keys) have to be
            represented by altering these seven notes with sharp signs (#) or
            flat signs (b), either in the key signature or as an accidental.
          </p>
          <p>
            The chromatic staff approach can make music easier to read, play,
            and understand by improving upon aspects of traditional notation
            such as <a href="#keys">key signatures</a>, <a href="#accidentals"
              >accidentals</a
            >, <a href="#clefs">clefs</a>, <a href="#octaves">octaves</a>, and <a
              href="#intervals">intervals</a
            >. The rest of this page shows how.
          </p>
          <h3>Many Different Kinds of Chromatic Staff</h3>
          <p>
            There are many variations on this chromatic staff theme, with a
            number of different line patterns (see <a href="/systems/"
              >Notation Systems</a
            >). We use a basic five-line version just to introduce the concept,
            not to suggest that it is the best one. We do not promote a
            particular alternative notation system, but document a variety of
            them and teach the general principles on which they are based.
          </p>
          <h2 id="keys" class="divider-heading">Key Signatures</h2>
          <div
            style="float: right; margin: 0; padding: 15px 5% 5px 5px; max-width: 390px; font-size: 90%;"
          >
            <span id="quote"
              >"The need for a new notation, or a radical improvement of the
              old, is greater than it seems, and the number of ingenious minds
              that have tackled the problem is greater than one might think." —
              Arnold Schoenberg</span
            >
            <a id="ftnref1" href="#ftn1">[1]</a>
          </div>
          <p>
            <img
              loading="lazy"
              style="border: 0px none;"
              src="/wp-content/uploads/2013/03/KeysTrebleBass2.gif"
              alt="A grand staff with bass and treble clefs illustrating all fifteen key signatures one by one in rotation"
              width="240"
              height="133"
            />
          </p>
          <p>
            There are fifteen different key signatures to memorize in
            traditional notation, and one must always keep the current key
            signature in mind while playing. With a chromatic staff this is not
            necessary since a note&#8217;s position on the staff directly
            indicates what note to play. All keys are equally easy to read.
          </p>
          <p>
            <button class="showHideButton" id="showHideButton0"
              >Show less about Key Signatures</button
            > | <button class="showHideButton" id="showHideAllButton"
              >Collapse all sections on this page</button
            >
          </p>
          <div id="hiddenText0">
            <p>
              The traditional staff is based upon the notes from the key of C
              major and closely related tonalities. This makes all of the other
              keys and tonalities more difficult to read, even if they are just
              as easy to play on a given instrument. To become proficient in all
              keys one must master fifteen different key signatures which are
              progressively more complex and difficult to learn.
            </p>
            <p>
              On a chromatic staff all of the keys are equally easy to read.
              There is no need to memorize and remember key signatures. Reading
              music is more direct as you simply play the notes that you see,
              without having to filter them through a memorized key signature.
            </p>
            <p>
              In traditional notation, key signatures not only alter the pitches
              of the lines and spaces but also alert the reader to the
              music&#8217;s predominant key. With a chromatic staff, a much
              simpler alternative key signature system can be used for the
              latter purpose, helping the musician know which notes to expect.
            </p>
          </div>
          <h2 id="accidentals" class="divider-heading">Accidentals</h2>
          <p>
            <img
              loading="lazy"
              style="border: 0px none;"
              src="/wp-content/uploads/2013/03/accidentalsTreble.png"
              alt="Notes with five different accidental signs: flat, sharp, double flat, double sharp, and natural on a standard five line staff"
              width="460"
              height="85"
            />
          </p>
          <p>
            In traditional notation accidental signs must be used to represent
            accidentals — notes that are not in the current key signature. On a
            chromatic staff they are not needed since all notes have their own
            position on the staff, making it easier to see each note&#8217;s
            pitch and how it relates to other notes.
          </p>
          <p>
            <button class="showHideButton" id="showHideButton1"
              >Show less about Accidentals</button
            >
          </p>
          <div id="hiddenText1">
            <p>
              Music that requires many accidental signs in traditional notation
              can be visually challenging to read, especially if it is in an
              unfamiliar and complex key signature. The rule that an accidental
              sign applies to a given pitch until the end of the measure, unless
              it is cancelled by another one, opens up room for mistakes and
              gives the musician one more thing to remember. There is often
              confusion about whether an accidental applies to other octaves of
              the same note (it generally does not).
            </p>
            <p>
              On a chromatic staff accidental signs are not needed because each
              of the twelve notes of the chromatic scale has its own unique
              position on the staff, regardless of whether it is in the
              prevailing key. This makes it easier to identify the pitch of
              accidental notes and to see how they relate to surrounding notes.
            </p>
            <p>
              Optional &#8220;alternative accidental signs&#8221; may be used to
              simply indicate that a note falls outside of the current key
              (without affecting the note&#8217;s pitch), or they may be used to
              distinguish between enharmonically equivalent notes if desired
              (see <a href="/tutorials/enharmonic-equivalents/"
                >Enharmonic Equivalents</a
              > tutorial).
            </p>
          </div>
          <h2 id="clefs" class="divider-heading">Clefs</h2>
          <p>
            <img
              loading="lazy"
              style="border: 0px none;"
              src="/wp-content/uploads/2013/03/Clefs8.png"
              alt="standard musical staff illustrating the treble, bass, alto, and tenor clefs with notes at the same position on the staff changing pitch for each clef (E, G, F, D)"
              width="570"
              height="91"
            />
          </p>
          <p>
            In traditional notation, staves that look the <em>same</em> may represent
            <em>different</em> sets of notes, depending on the clef symbol. On a
            typical chromatic staff the lines and spaces always represents the same
            notes, and an octave or register symbol simply indicates the staff&#8217;s
            pitch range.
          </p>
          <p>
            <button class="showHideButton" id="showHideButton2"
              >Show less about Clefs</button
            >
          </p>
          <div id="hiddenText2">
            <p>
              Traditional notation is commonly written in four different clefs:
              treble, bass, alto, and tenor. Each clef changes which notes are
              represented by the lines and spaces of the staff. Keyboard players
              have the additional challenge of learning to read both treble and
              bass clefs at the same time. Key signatures appear at a different
              vertical position in each of the different clefs (see <a
                href="/#keys">key signatures</a
              > above).
            </p>
          </div>
          <h2 id="octaves" class="divider-heading">Octaves</h2>
          <p>
            <img
              loading="lazy"
              style="border: 0px none;"
              src="/wp-content/uploads/2013/03/TraditionalOctaves.png"
              alt="standard musical staff illustrating the treble, bass, alto, and tenor clefs with notes at the same position on the staff changing pitch for each clef (E, G, F, D)"
              width="570"
              height="150"
            />
          </p>
          <p>
            On a traditional staff, two notes an octave apart do not look alike.
            If a note falls on a <em>line</em>, the note an octave higher or
            lower will fall on a <em>space</em> (and vice-versa).<br />
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/octaves-chromatic-5-line.png"
              alt="A two-octave five-line chromatic staff showing the notes E, G, F, and D and how they appear at the same place on the staff in both octaves"
              width="570"
              height="150"
            />
          </p>
          <p>
            On chromatic staves like the one above, notes an octave apart look
            the same. Notes are easy to identify since a given note always has
            the same appearance regardless of its octave.
          </p>
          <p>
            <button class="showHideButton" id="showHideButton3"
              >Show less about Octaves</button
            >
          </p>
          <div id="hiddenText3">
            <p>
              The traditional staff is based on an <em>odd</em> number of seven notes
              per octave (a diatonic scale), which is why notes an octave apart do
              not both appear on lines (or both on spaces). Chromatic staves are
              based on an <em>even</em> number of twelve notes per octave (a chromatic
              scale), so notes an octave apart can both fall on lines (or both on
              spaces).
            </p>
            <p>
              Most chromatic staves &#8220;cycle on the octave&#8221; so that
              each octave looks the same. This allows them to be
              &#8216;stacked&#8217; to form a continuous staff that spans two or
              more octaves as shown above. This accommodates the different pitch
              ranges of various instruments and reduces the need to use ledger
              lines.
            </p>
            <p>
              (Note that alternative notation systems that use a diatonic staff
              with irregularly spaced lines can also cycle on the octave so that
              notes an octave apart look the same. See for example, <a
                href="http://nydana.se"
                target="_blank">Nydana Notation</a
              >.)
            </p>
          </div>
          <h2 id="intervals" class="divider-heading">Intervals</h2>
          <p>
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/scale-3rds1.png"
              alt="Standard musical staff showing an ascending major scale with whole steps and half steps labelled"
              width="452"
              height="231"
            />
          </p>
          <p>
            Intervals that look the same in traditional notation may not be the
            same interval. In this illustration, whole steps and half steps are
            visually indistinguishable, as are major and minor thirds. What you <em
              >see</em
            > does not always correspond with what you <em>hear</em> and must <em
              >play</em
            >.<br />
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/scale-chromatic-intervals.png"
              alt="Five-line chromatic staff with a C major scale and whole steps and half steps labelled"
              width="450"
              height="139"
            />
          </p>
          <p>
            Chromatic staves represent intervals more consistently and more
            accurately. As shown in this illustration of a C major scale, whole
            steps are always two notes on two neighboring lines or on two
            neighboring spaces. Half steps are always one note on a line and
            another on a neighboring space.
          </p>
          <p>
            <button class="showHideButton" id="showHideButton4"
              >Show less about Intervals</button
            >
          </p>
          <div id="hiddenText4">
            <p>
              Traditional notation obscures the interval relationships between
              notes. For example, a major scale (a series of whole steps and
              half steps, as shown above) appears as a regular sequence of
              notes, while a whole tone scale (a series of identical whole
              steps) appears as an irregular sequence.
            </p>
            <p>
              To fully identify an interval one must take into account the
              current clef sign, key signature, and any accidentals, going
              through the mental procedure of calculating the names of the
              individual notes before their interval relationship can be
              determined.
            </p>
            <p>
              Chromatic staves give a consistent and accurate representation of
              the intervals between notes. This clearer representation of
              intervals makes it much easier to recognize the interval patterns
              that make up various scales, or any interval pattern prevalent
              within a given musical genre or tradition. It also simplifies the
              comprehension of harmony and music theory, for example, allowing
              easy recognition of major and minor thirds and the chords that are
              built from them. Finally, it would help with learning to improvise
              or play by ear since these are skills that entail playing by
              interval relationships.
            </p>
            <p>
              It may seem that chromatic staves are only suited for atonal
              music, but in fact they work well with all types of music:
              traditional or modern, tonal or atonal, etc. They are actually
              much better than traditional notation at representing the diatonic
              interval patterns that are the basis of tonal music. Diatonic
              interval patterns are built into the traditional staff, obscured
              from view and from awareness, but a chromatic staff makes them
              explicit, easy to perceive, and simple to understand.
            </p>
            <p>
              See our <a href="/tutorials/intervals/">Intervals Tutorial</a> for
              more extensive illustrations and discussion.
            </p>
          </div>
          <h2 class="divider-heading">A Better Approach to Music Notation</h2>
          <p>
            All of these features of traditional music notation combine to make
            reading music much more difficult than it might be with a better
            notation system. For an analogy, imagine trying to do arithmetic
            with <a href="http://en.wikipedia.org/wiki/Roman_numerals"
              >Roman numerals</a
            >. It can be done, but the notation system makes a big difference.
            Of course it is important to view traditional notation in its
            broader historical context and to keep in mind the innovations and
            reforms that it has undergone over time. <a
              id="ftnref2"
              href="#ftn2">[2]</a
            >
          </p>
          <p>
            Alternative music notation systems with chromatic staves avoid each
            of these pitch-related difficulties, and offer significant
            advantages over traditional music notation.<a
              id="ftnref3"
              href="#ftn3">[3]</a
            >
          </p>
          <p>&nbsp;</p>
          <h2 class="divider-heading">Next</h2>
          <p>
            <strong><a href="/systems/">Notation Systems</a></strong> — Check out
            some alternative music notation systems.
          </p>
          <p>
            <strong><a href="/tutorials/">Tutorials</a></strong> — Learn about various
            approaches to alternative music notation.
          </p>
          <p>&nbsp;</p>
          <div class="footnotes divider-section">
            <p>
              <a id="ftn1" href="#quote">[1]</a> Arnold Schoenberg was probably the
              most influential 20th-century composer of Western &#8220;classical
              music.&#8221; It is less commonly known that he also invented a <a
                href="/system/untitled-by-arnold-schoenberg/"
                >chromatic staff notation system</a
              >. <a href="/#quote">This quote</a> is from his &#8220;A New Twelve-Tone
              Notation,&#8221; written in 1924 (see
              <a
                href="http://books.google.com/books?id=K9lMP5oqKEgC"
                target="_blank"
                ><em>Style and Idea: Selected Writings of Arnold Schoenberg</em>
              </a>). Although Schoenberg was a proponent of atonal, non-diatonic
              music, his statement is relevant for all kinds of music. Most
              alternative notation systems were invented primarily with
              traditional tonal music in mind, and we are interested in making
              all types of music easier to read and play. Though we quote
              Schoenberg we do not discount the importance of diatonic scales
              and keys. Chromatic staff notation systems actually represent
              diatonic scales, tonalities, and their intervals much more
              faithfully than traditional notation. See <a href="/#intervals"
                >Intervals</a
              > on this page, and our <a href="/tutorials/intervals/"
                >Intervals Tutorial</a
              >.
            </p>
            <p>
              <a id="ftn2" href="#ftnref2">[2]</a> Traditional notation was developed
              over several centuries for use with music and instruments that were
              different from those of today. <a
                href="http://en.wikipedia.org/wiki/Guido_of_Arezzo"
                target="_blank">Guido d&#8217;Arezzo</a
              > introduced his staff-based system in about 1025 CE, but the five-line
              staff only became standardized in the 1500s. This staff-based notation
              was a significant achievement that improved upon the notation systems
              that preceded it, and it has continued to evolve over time to address
              new notational needs as they arose.
            </p>
            <p>
              <a id="ftn3" href="#ftnref3">[3]</a> One of the few disadvantage to
              chromatic staves is that they typically require more vertical space
              on the page, since they include five additional notes per octave. Some
              notation systems solve this problem, for example <a
                href="/systems/group/major-3rd-2-lines-compact/"
                >systems with more than one notehead shape</a
              > or systems with a compressed staff such as <a
                href="/system/express-stave-by-john-keller/">Express Stave</a
              >, <a href="/system/clairnote-sn-by-paul-morris/">Clairnote SN</a
              >, and <a href="/system/clairnote-dn-by-paul-morris/"
                >Clairnote DN</a
              >.
            </p>
          </div>
        </div>
      </article>
    </div>
  </div>
</MnpLayout>

<style>
  .showHideButton {
    cursor: pointer;
    color: rgb(52, 120, 200);
    background: none;
    border: none;
    padding: 0;
    font-size: 100%;
  }
  .showHideButton:hover {
    text-decoration: underline;
  }
</style>

<script>
  type BlockOrNone = "block" | "none";

  let allBlockOrNone: BlockOrNone = "block";

  type ToggleButtonConfig = {
    sectionId: string;
    buttonId: string;
    hiddenText: string;
    shownText: string;
  };

  const toggleButtonConfigs = [
    {
      sectionId: "hiddenText0",
      buttonId: "showHideButton0",
      hiddenText: "Show more about key signatures",
      shownText: "Show less about key signatures",
    },
    {
      sectionId: "hiddenText1",
      buttonId: "showHideButton1",
      hiddenText: "Show more about accidentals",
      shownText: "Show less about accidentals",
    },
    {
      sectionId: "hiddenText2",
      buttonId: "showHideButton2",
      hiddenText: "Show more about clefs",
      shownText: "Show less about clefs",
    },
    {
      sectionId: "hiddenText3",
      buttonId: "showHideButton3",
      hiddenText: "Show more about octaves",
      shownText: "Show less about octaves",
    },
    {
      sectionId: "hiddenText4",
      buttonId: "showHideButton4",
      hiddenText: "Show more about intervals",
      shownText: "Show less about intervals",
    },
  ] as const;

  const toggleHidden = (
    { sectionId, buttonId, hiddenText, shownText }: ToggleButtonConfig,
    blockOrNone?: BlockOrNone
  ) => {
    const section = document.getElementById(sectionId);
    const button = document.getElementById(buttonId);

    if (section && button) {
      section.style.display =
        blockOrNone ?? (section.style.display === "none" ? "block" : "none");

      button.innerHTML =
        section.style.display === "none" ? hiddenText : shownText;
    } else {
      console.error(
        `not found: section (${sectionId}) or button (${buttonId})`
      );
    }
  };

  const toggleAll = () => {
    const showHideAllButton = document.getElementById("showHideAllButton");

    if (showHideAllButton) {
      showHideAllButton.innerHTML =
        allBlockOrNone === "none"
          ? "Collapse all sections on this page"
          : "Expand all sections on this page";

      allBlockOrNone = allBlockOrNone === "none" ? "block" : "none";
    } else {
      console.error("not found: showHideAllButton");
    }
    toggleButtonConfigs.forEach((config) =>
      toggleHidden(config, allBlockOrNone)
    );
  };

  window.addEventListener("load", () => {
    toggleAll();

    document
      .getElementById("showHideAllButton")
      ?.addEventListener("click", () => toggleAll());

    toggleButtonConfigs.forEach((config) => {
      document
        .getElementById(config.buttonId)
        ?.addEventListener("click", () => toggleHidden(config));
    });
  });
</script>
