---
import MnpLayoutWiki from "../../../../layouts/MnpLayoutWiki.astro";
import { makeMetaTitle } from "../../../../utils/utils";
---

<MnpLayoutWiki
  metaTitle={makeMetaTitle("Isomorphism", "Wiki")}
  metaDescription="Isomorphism in music notation, where any given musical pattern (scale, chord, interval, melody, etc.) has the same basic appearance at different transpositions."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="wiki_page type-wiki_page status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Isomorphism</h1>
        </header>

        <div class="entry-content">
          <p>
            The term &#8220;isomorphic&#8221; is <a
              href="http://www.merriam-webster.com/dictionary/isomorphic"
              rel="nofollow">defined</a
            > as &#8220;being of identical or similar form, shape, or structure.&#8221;
          </p>
          <p>
            The idea of &#8220;isomorphism&#8221; as applied to music notation
            is that any given musical pattern (like a scale, chord, interval, or
            particular melody) should always have the same basic appearance
            regardless of which pitch it starts on. There should be consistency
            in the relationship between what you <i>see</i> and what you <i
              >hear</i
            >.
          </p>
          <p>
            Each aspect of music — notation, nomenclature, and instruments — can
            be simplified through isomorphism. Using an isomorphic notation
            system with an isomorphic nomenclature and/or an isomorphic
            instrument so that these different aspects match each other, would
            likely result in added benefits.
          </p>
          <p>
            There are several basic isomorphic patterns, different ways of
            distinguishing the twelve notes of the chromatic scale. (Numbers are
            used in the table below just to illustrate the different patterns in
            a formal, abstract way.)
          </p>
          <p>&nbsp;</p>
          <table>
            <tbody>
              <tr>
                <th>Basic Isomorphic Patterns</th>
                <th>A</th>
                <th
                  >A#<br />
                  Bb</th
                >
                <th>B</th>
                <th>C</th>
                <th
                  >C#<br />
                  Db</th
                >
                <th>D</th>
                <th
                  >D#<br />
                  Eb</th
                >
                <th>E</th>
                <th>F</th>
                <th
                  >F#<br />
                  Gb</th
                >
                <th>G</th>
                <th
                  >G#<br />
                  Ab</th
                >
              </tr>
              <tr>
                <td
                  >6-6 pattern (binary), cycles every 2 semitones (major
                  second).</td
                >
                <td>1</td>
                <td>2</td>
                <td>1</td>
                <td>2</td>
                <td>1</td>
                <td>2</td>
                <td>1</td>
                <td>2</td>
                <td>1</td>
                <td>2</td>
                <td>1</td>
                <td>2</td>
              </tr>
              <tr>
                <td
                  >4-4-4 pattern (tertiary), cycles every 3 semitones (minor
                  third).</td
                >
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
              </tr>
              <tr>
                <td
                  >3-3-3-3 pattern (quaternary), cycles every 4 semitones (major
                  third).</td
                >
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
              </tr>
              <tr>
                <td
                  >2-2-2-2-2-2 pattern (senary), cycles every 6 semitones
                  (tritone).</td
                >
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>5</td>
                <td>6</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>5</td>
                <td>6</td>
              </tr>
            </tbody>
          </table>
          <p>
            See also: <a
              href="/tutorials/6-6-and-7-5-pitch-patterns/"
              rel="nofollow">6-6 and 7-5 Pitch Patterns Tutorial</a
            > and <a href="/wiki/music-theory/4-4-4-pitch-pattern/"
              >4-4-4 Pitch Pattern</a
            >.
          </p>
          <p>&nbsp;</p>
          <h3>Notation Systems</h3>
          <p>
            Isomorphic notation systems are pitch-proportional and have
            regularly repeating patterns. This typically takes the form of a
            regular line pattern with a consistent interval distance between
            each line. For example, notations with lines a major second apart
            have a 6-6 line pattern, notations with lines a minor third apart
            have a 4-4-4 line pattern, and notations with lines a major third
            apart have a 3-3-3-3 line pattern. Isomorphism can also be achieved
            through the use of alternating hollow and solid notes (a 6-6
            pattern) or different patterns of notehead shapes.
          </p>
          <p>&nbsp;</p>
          <h3>Nomenclatures</h3>
          <p>
            The traditional musical nomenclature (note names, interval names,
            etc) is tied to the same complicated framework as traditional
            notation (sharps and flats, enharmonic equivalents, etc). So there
            is good reason to consider new nomenclatures that do not have these
            complications. There are also benefits to using a nomenclature that
            correlates well with a given alternative, isomorphic notation
            system.
          </p>
          <p>
            (Of course, there are also benefits to having a standard
            nomenclature for easy communication between musicians. For that
            reason is probably easier for an individual to adopt an alternative
            notation system or instrument, than it would be to adopt an
            alternative nomenclature.)
          </p>
          <p>&nbsp;</p>
          <h3>Instruments</h3>
          <p>
            Instruments are isomorphic when the same musical pattern can be
            played in the same basic way regardless of the starting pitch.
            Common examples of isomorphic instruments are stringed instruments
            like the violin, viola, cello, string bass, bass guitar, and
            mandolin. (The guitar is mostly isomorphic, but in its usual tuning
            it has a slightly irregular pattern, because the interval between
            the G and B strings is a major third while other neighboring strings
            are all separated by a perfect fourth. Some advocate tuning the
            guitar completely in either perfect fourths or major thirds for this
            reason.)  The traditional piano keyboard is not isomorphic (it has a
            7-5 pattern), so it is necessary to learn many different fingerings
            to play the same scale or chord when starting on different pitches.
            A number of new keyboard layouts and instrument designs have been
            introduced to provide the benefits of isomorphism. See <a
              href="/wiki/instruments/isomorphic-instruments/"
              >Isomorphic Instruments</a
            >.
          </p>
          <p>
            There are benefits to using instruments that correlate well with a
            given notation and nomenclature. This is why tablature is popular:
            the notation matches the instrument. It is also why isomorphic
            instruments are a logical companion to isomorphic notation systems.
            That is why many people interested in isomorphic notation systems
            are also interested in isomorphic instruments.
          </p>
        </div>
      </article>
    </div>
  </div>
</MnpLayoutWiki>
