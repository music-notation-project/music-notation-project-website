---
import MnpLayoutWiki from "../../../../layouts/MnpLayoutWiki.astro";
import { makeMetaTitle } from "../../../../utils/utils";
---

<MnpLayoutWiki
  metaTitle={makeMetaTitle("Slantnote by David Zethmayr", "Wiki")}
  metaDescription="Documentation of Slantnote, an alternative music notation system by David Zethmayr."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="wiki_page type-wiki_page status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Slantnote by David Zethmayr</h1>
        </header>

        <div class="entry-content">
          <h2>Slantnote-<sub>bis</sub> Intro for Students</h2>
          <p>
            <img
              loading="lazy"
              alt="Teapot rails-sq-clef-C.png"
              src="/wp-content/uploads/2013/03/800px-Teapot_rails-sq-clef-C.png"
              width="800"
              height="202"
            />
          </p>
          <p>
            The <a
              href="/wiki/music-theory/the-squiggle-method-of-pitch-visualization/"
              >Squiggle</a
            >&#8211;that diagram between the two phrases above, with the
            footprints&#8211;will leap-frog you over the worst beginning
            difficulties of music reading and music theory. There are
            many-plenty <a
              href="http://earfirst.com/folk/index.html"
              rel="nofollow"
              ><strong
                >examples of Slantnote-<sub>bis</sub> at EarFirst.com</strong
              >
            </a> for practice on familiar songs. <big
              ><strong>Watchit!</strong>
            </big> the <strong>examples</strong> link has some files that do NOT
            use either <em>slantnote</em> or <em>slantmarkup</em>. You have to
            read the short KEY TO FILE TYPES at the top to decide which ones to
            download.
          </p>
          <h2>Slantnote-<sub>bis</sub> Intro for Educators</h2>
          <p>
            <img
              loading="lazy"
              alt="ThreeBlindMice rails&sq.png"
              src="/wp-content/uploads/2013/03/760px-ThreeBlindMice_rails%26sq.png"
              width="760"
              height="126"
            />
          </p>
          <p>
            <em>Slantnote</em> notations and markups are based on the <em
              >squiggle</em
            >, a vertical alternative to the keyboard for visualizing melodic
            and harmonic structures. The squiggle and its notations are
            motivated by weaknesses in conventional notation in the areas of
            fast sight-singing and of amateur self-education for accurate
            reading <em>a cappella</em>.
          </p>
          <h3>Slantnote<sub>-bis</sub> on Rails</h3>
          <p>
            <em>Slantnote on rails</em> uses a staff of <em>rails</em>: tonic
            rails and dominant rails only. Rails are key-free moveable in the
            same sense that &#8220;moveable-do&#8221; solfeggio is not locked to
            a specific pitch. Usually they are used in threes, in either <em
              >authentic</em
            > or <em>plagal lie</em>. When tonic rails are at bottom and top the
            arrangement is in <em>authentic lie</em>, with a dominant rail
            intermediate—4/7 the tonic octave separation above the lower tonic.
          </p>
          <p>
            <img
              loading="lazy"
              alt="Rails in authentic lie"
              src="/wp-content/uploads/2013/03/700px-Chromatic-authentic-rails.png"
              width="700"
              height="145"
            />
          </p>
          <p>
            The reverse arrangement is <em>plagal lie</em>: dominant rails
            outermost, with tonic intermediate at 3/7 the octave distance.
          </p>
          <p>Leger lines are likewise dominant or tonic only.</p>
          <p>
            <img
              loading="lazy"
              alt="Rails in plagal lie"
              src="/wp-content/uploads/2013/03/700px-Chromatic-plagal-rails.png"
              width="700"
              height="186"
            />
          </p>
          <h3>Slantnote<sub>-bis</sub> on Clefs</h3>
          <p>
            <em>Slantnote on clefs</em> uses the well-known 5-line staff with clef
            sign and key signature, much as on rails. In addition to slanting the
            noteheads, the 3-2-1 footprint follows immediately after the key signature.
          </p>
          <p>
            The slant of notes, and the small marks on né-slant notes, can be
            easily ignored by those who don&#8217;t want to use them because
            they already know how to read in 5-line clefs. On the other hand,
            bright beginners can note their import and use them to settle the <strong
              >F</strong
            >inal <strong>S</strong>emitone <strong>U</strong>ncertainty,
            without tutelage.
          </p>
          <p>
            Generations of amateur choir members have taught themselves to
            &#8220;almost-read&#8221; in clefs, relying on Fickle Dame Memory
            and someone pounding out the <strong>FSU</strong> on a piano. This, unfortunately,
            also <strong>teaches</strong> that a keyboardist is necessary for rehearsals
            and that diatonic theory is beyond the reach of those not having studied
            some instrument.
          </p>
          <p>
            Further, the practice habituates both choir and accompanist to
            too-loud performance, destroying the possibility of solid
            intonation, since that depends on hearing oneself and the ensemble
            at once.
          </p>
          <h3>Slantmarkup</h3>
          <p>
            The third squiggle-informed notation, <em>slantmarkup</em>, is the
            most conservative in respect of usual notation. It is a markup
            schema that can be retrofitted to existing conventional scores as
            redundant information in aid to sight-reading precision.
          </p>
          <p>
            <img
              loading="lazy"
              alt="Slantmarkup HappyTrails.png"
              src="/wp-content/uploads/2013/03/790px-Slantmarkup_HappyTrails.png"
              width="790"
              height="206"
            />
          </p>
          <p>
            In slantmarkup, first a squiggle <em>footprint</em> outlining the 1-2-3
            degrees locates (<em><strong>foots</strong></em>) the diatonic
            reference on the staff. The squiggle footprint serves as a
            &#8220;key signature decode&#8221;. (On &#8220;rails&#8221;, the
            footprint tells the mode.)
          </p>
          <p>
            Once the diatonic pattern is thus footed, a small slant is drawn
            next to the printed notehead of notes referable to the <em>né</em> side
            (footprint side) of the squiggle (the opposite side is the <em
              >nà</em
            > side; <em>nà</em> notes are left unmarked). Vowel accents over <em
              >né</em
            > and <em>nà</em> reflect the slant orientation. Squiggle <em>né</em
            > side and squiggle <em>nà</em> side are right-left reversible. The side
            where 3-2-1 diatonic degrees happen to reside is always the <em
              >né</em
            > side.
          </p>
          <h2>Solfeggio</h2>
          <h3>Squiggle Solfege—&#8221;né-nà&#8221;</h3>
          <p>
            <img
              loading="lazy"
              alt="Solfege Squiggle.png"
              src="/wp-content/uploads/2013/03/120px-Solfege_Squiggle.png"
              width="120"
              height="168"
            />
          </p>
          <p>
            The two squiggle <em>cis-trans</em> syllables make up squiggle solfeggio,
            or <em>né-nà-feggio</em>. The diatonic-7 pattern 1-2-3-4-5-6-7,
            solmized <em>do-re-mi-fa-so-la-ti</em> US style, is rendered <em
              >né-né-né-nà-nà-nà-nà</em
            > in squiggle solfege, because <em>né</em>-side squiggle is where
            the &#8220;/Three Blind Mice/&#8221; hang out. Also
            &#8220;/Mary&#8221; and her &#8220;Little Lamb/&#8221; and
            &#8220;/Yankee Doodle Went to Town/&#8221;.
          </p>
          <h3>Rhyming Major Thirds Solfege</h3>
          <p>
            <img
              loading="lazy"
              alt="Solfege rM3.png"
              src="/wp-content/uploads/2013/03/150px-Solfege_rM3.png"
              width="150"
              height="164"
            />
          </p>
          <p>
            A solfeggio minimally changed from <em>Sound of Music</em> &#8220;Doe,
            a Deer&#8221; style is RM3, where major thirds rhyme throughout <em
              ><strong>and minor thirds reliably fail to rhyme</strong>
            </em> (sopranos take especial note!). Using the vowel sequence o-à-é-i
            (ascending semitones) in cyclic fashion, the major scale is rendered
            do-re-mo-fa-si-la-ti-do. Names of accidentals are, of course, adjusted
            with regard to the same semitone cycle.
          </p>
          <h2>MS Paper</h2>
          <p>
            For your convenience, this <a
              title="Reversible-rails-6ptMS.pdf"
              href="/wp-content/uploads/2013/03/Reversible-rails-6ptMS.pdf"
              >MS file</a
            > can be printed off to serve for either authentic or plagal transcription.
            Turn it upside down to reverse the lie.
          </p>
          <p>
            The rails manuscript paper is for both Slantnote-<sub>bis</sub> and Slantnote-<sub
              >tris</sub
            > transcription.
          </p>
          <p>
            <strong>Early Simplification</strong>: For beginning students who
            will not have to deal with the classical staff very soon, just two
            rails are enough: tonic low, dominant high. Lines of pre-college
            lined notebook paper are at suitable distance. When a tune goes to
            the low dominant or the high tonic, a leger line 3/4 that distance
            to the next printed line is drawn, both for notes
            &#8220;beaded&#8221; thereon and notes tangent thereto.
          </p>
          <p>&nbsp;</p>
          <p>
            &#8212; &#8220;Slantnote&#8221; (David Zethmayr), 3 February 2012
          </p>
        </div>
      </article>
    </div>
  </div>
</MnpLayoutWiki>
