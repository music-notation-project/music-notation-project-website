---
import MnpLayoutWiki from "../../../../layouts/MnpLayoutWiki.astro";
import { makeMetaTitle } from "../../../../utils/utils";
---

<MnpLayoutWiki
  metaTitle={makeMetaTitle("Kevin Dalley's LilyPond Code", "Wiki")}
  metaDescription="A wiki page about Kevin Dalley's work to get LilyPond to support alternative notation systems."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="wiki_page type-wiki_page status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Kevin Dalley's LilyPond Code</h1>
        </header>

        <div class="entry-content">
          <p>
            <em
              >This page documents some earlier important work by Kevin Dalley. 
              See the main <a href="/wiki/lilypond/">LilyPond wiki page</a> for the
              most current information.<br />
            </em>
          </p>
          <p>
            Kevin Dalley volunteered his time and skills to add features to
            LilyPond to support chromatic-staff notation systems.  He wrote a
            set of patches (initially for LilyPond version 2.11.0) that provided
            for basic chromatic staff functionality. Now most of these patches
            have been incorporated into Lilypond, as you can see <a
              href="http://git.savannah.gnu.org/gitweb/?p=lilypond.git&a=search&h=HEAD&st=author&s=Kevin+Dalley"
              target="_blank">here</a
            >. (He submitted a <a
              href="http://code.google.com/p/lilypond/issues/detail?id=1193#makechanges"
              target="_blank">patch for internal-ledger-lines</a
            > but it was never finished. However, it has been superseded by a newer
            patch by another developer that adds this functionality to LilyPond.)
          </p>
          <p>
            Mark Hanlon also contributed by updating and maintaining
            Kevin&#8217;s code so that it would continue to work with more
            recent versions of LilyPond (although it is no longer compatible
            with newer LilyPond releases). Andrew Wagner set up a git repository
            for the patches and additional files on <a
              href="http://github.com/"
              target="_blank">github.com</a
            > to help organize and facilitate future work.
          </p>
          <h2>How to Use Dalley&#8217;s &#8220;chromatic.ly&#8221; Approach</h2>
          <p>
            Most of the content on this page comes from Dalley&#8217;s <a
              href="http://www.kelphead.org/chromatic/"
              target="_blank">website</a
            >, and documents his approach to using LilyPond for alternative
            notation systems &#8212; using his &#8220;chromatic.ly&#8221; and
            template files. There are some non-functioning features (see below).
          </p>
          <p>
            It is also possible to use LilyPond for alternative notation systems
            without using Kevin&#8217;s &#8220;chromatic.ly&#8221; and template
            files. See the main <a href="/wiki/lilypond/">LilyPond wiki page</a>
            for more about working more directly with LilyPond rather than using
            Dalley&#8217;s files. (Both of these approaches are possible because
            of the code that Dalley contributed to LilyPond.)
          </p>
          <p>&nbsp;</p>
          <h3>
            Using LilyPond to transnotate music into an alternative notation
          </h3>
          <p>
            Assuming you have already installed LilyPond, here is how you would
            use it to transnotate music into an alternative notation.
          </p>
          <ol>
            <li>
              You will need a LilyPond music file (.ly) in standard LilyPond
              format, in a form similar to <a
                href="/wp-content/uploads/2013/03/bachinvention09common.ly"
                >bachinvention09common.ly</a
              >. The <a href="http://www.lilypond.org" target="_blank"
                >LilyPond website</a
              > explains how to write such files from scratch. You can also download
              them from the <a
                href="http://www.mutopiaproject.org/"
                target="_blank">Mutopia project</a
              > or other online sources.
            </li>
            <li>
              You will need a copy of the  <a
                href="/wp-content/uploads/2013/03/chromatic.ly">chromatic.ly</a
              > file which contains the definitions of various alternative music
              notation systems.
            </li>
            <li>
              You will also need a template file like this one: <a
                href="/wp-content/uploads/2013/03/bachinvention09template.ly"
                >bachinvention09template.ly</a
              >
            </li>
            <li>
              Open the template in any basic text editor and change the
              following line to refer to your LilyPond music file:\include
              &#8220;bach-invention-09-common.ly&#8221;
            </li>
            <li>
              Next edit the following line near the top of the template so it
              refers to your desired notation. (See below for a list of notation
              system names that can be entered here. Each system is defined in
              the chromatic.ly file.)#(define notation-style
              &#8220;6-6-tetragram&#8221;)
            </li>
            <li>
              In the template, change these lines to define how many octaves
              your music covers.#(define lower-octave -2)<br />
              #(define upper-octave 1)
            </li>
            <li>
              In the template, the following should be edited so they are
              appropriate for your file. You can add another voice, or another
              staff, but should consult the LilyPond documentation for details
              on this.\VoiceOne<br />
              \VoiceTwoNoClef
            </li>
            <li>
              Now open the template file with LilyPond. It should output your
              music file in the designated alternative notation, except for the
              following non-functioning features.
            </li>
          </ol>
          <h3></h3>
          <h3><a id="caveats"></a>Non-Functioning Features</h3>
          <p>
            When used with the standard LilyPond application, Dalley&#8217;s
            &#8220;chromatic.ly&#8221; and template files do not currently
            render ledger lines that are internal to the staff, or different
            note head shapes. Dalley created a modified version of LilyPond that
            provided these two features, but they were not incorporated into the
            official LilyPond application. (See below for more about his patches
            to LilyPond.)
          </p>
          <p>
            LilyPond now has support for internal ledger lines, so at some point
            it would be worth updating Dalley&#8217;s files to use this feature
            (rather than the one he wrote in his custom version of LilyPond).
            Different note head shapes can also be achieved. (See the main <a
              href="/wiki/lilypond/">LilyPond wiki page</a
            >, for more about these features.)  So there is work to be done
            integrating Dalley&#8217;s chromatic.ly and template files / code /
            method with the current version of LilyPond.
          </p>
          <p>&nbsp;</p>
          <h3>Notation Systems</h3>
          <p>
            Here is a list of notation names that can be entered in the template
            in step four above. Each name corresponds to a particular notation
            system (or &#8220;notation-style&#8221;). Note that many of these
            are <em>not</em> fully implemented and are only approximations or first
            steps towards full support of a given notation system. This list is defined
            in the chromatic.ly file (see below).
          </p>
          <p>
            <strong>6-6-tetragram</strong> (6-6 Tetragram by Richard Parncutt)<br
            />
            <strong>a-b</strong> (Albert Brennink&#8217;s Ailler-Brennink notation)<br
            />
            <strong>ailler</strong> (Johann Ailler&#8217;s 4-line notation)<br
            />
            <strong>5-line</strong> (a basic 5-line chromatic staff)<br />
            <strong>frix</strong> (Grace Frix&#8217;s 5-line chromatic staff)<br
            />
            <strong>avique</strong> (Anne &amp; Bill Collins&#8217; Avique notation)<br
            />
            <strong>mirck</strong> (Klavar, Mirck version by Jean de Buur)<br />
            <strong>twinline</strong> (Twinline, by Tom Reed)<br />
            <strong>twinline-2</strong> (Kevin Dalley&#8217;s experimental version
            of Twinline)<br />
            <strong>beyreuther-untitled</strong> (Johannes Beyreuther&#8217;s Untitled
            notation)<br />
            <strong>isomorph</strong> (Isomorph notation by Tadeusz Wojcik)<br
            />
            <strong>kevin</strong> (an experimental notation by Kevin Dalley)<br
            />
            <strong>express</strong> (Express Stave notation by John Keller)
          </p>
          <p>&nbsp;</p>
          <p>
            Note that your music file may need to be slightly edited beforehand
            to produce the desired result. For instance, when Dalley
            transnotated <a
              href="http://www.mutopiaproject.org/cgibin/piece-info.cgi?id=171"
              target="_blank">J. S. Bach&#8217;s Invention 9</a
            > into various notation systems, he had to make a few changes to the
            original file that he had downloaded from the <a
              href="http://mutopiaproject.org/"
              target="_blank">Mutopia project</a
            >. For the most part, the note section remained the same. However,
            the lower staff in the original switched between bass and treble
            clefs, which he had to modify before transnotating. Here is his
            slightly modified file for reference: <a
              href="/wp-content/uploads/2013/03/bachinvention09common.ly"
              >bachinvention09common.ly</a
            >
          </p>
          <p>
            (Eventually it would be nice to create a simple front-end
            application with a graphical user interface to handle most of these
            steps. Then the user would not have to directly edit the template
            file at all. The user could be presented with a window where they
            could specify the music file, select the desired notation system,
            define a few parameters like the number of octaves required, and
            click a button to transnotate the file with LilyPond.)
          </p>
          <div>
            <p>
              Note that Dalley also created a script that allows one to easily
              generate sheet music in many different notation systems at the
              same time. This &#8220;batch processing&#8221; script is not
              currently documented here, but it can be found on <a
                href="http://www.kelphead.org/chromatic/">his website</a
              >.
            </p>
            <h2>For Developers</h2>
            <p>
              This section has detailed information for programmers and anyone
              who is interested in what is going on &#8220;under the hood&#8221;
              with Dalley&#8217;s approach.  Most of the information below
              originally came from Dalley&#8217;s <a
                href="http://www.kelphead.org/chromatic/"
                target="_blank">website</a
              >.
            </p>
            <h3>
              How different alternative notation systems are defined and
              supported
            </h3>
            <p>
              Supported notation systems are defined in a table that is
              contained in the file <a
                href="/wp-content/uploads/2013/03/chromatic.ly">chromatic.ly</a
              >. This table makes it possible to quickly transnotate a given
              music file into any supported notation system. To add support for
              additional notation systems one would modify the file in order to
              add more notation systems to the table.
            </p>
            <p>
              The table contains the following information, which is sufficient
              to describe a wide range of systems. This information would be
              needed for any notation system that was to be added.
            </p>
            <dl>
              <dt>staff</dt>
              <dd>A numeric list of each staff line position in an octave.</dd>
              <dt>ledger</dt>
              <dd>A numeric list of each ledger line position in an octave.</dd>
              <dt>middleCPosition</dt>
              <dd>
                A number stating the position of middle C. (This lets you shift
                all the pitches of the staff up or down by changing the center
                pitch to a higher or lower note.)
              </dd>
              <dt>g-clef-from-c</dt>
              <dd>
                The numeric distance between C and G, which is where the G clef
                is positioned.
              </dd>
              <dt>positions-per-octave</dt>
              <dd>The number of distinct positions in an octave.</dd>
              <dt>layout</dt>
              <dd>
                Describes how notes are positioned. Currently either
                &#8220;semitone&#8221; or &#8220;twinline&#8221;.
              </dd>
              <dt>shape-note-styles</dt>
              <dd>
                For notations which have changing noteheads, a list of each
                notehead for each of the 12 semitones, starting with C.
              </dd>
            </dl>
            <p>&nbsp;</p>
            <p>
              The notation systems that are currently supported (and included in
              the table) are listed above. More systems could be easily added,
              but some systems cannot yet be fully supported since certain
              features are not yet possible. For example, automatically using
              black and white note heads for pitch, bold or dotted staff lines,
              or automatically placing notes on a particular side of the stem. 
              (These things can be done with LilyPond.  They just have not been
              integrated with Dalley&#8217;s code.  See the main LilyPond wiki
              page.)
            </p>
            <p>
              Note that the average user does not have to be able to read or
              modify the chromatic.ly file and its table of notations. Once a
              notation system has been set up, any user can transnotate music
              into it by calling it from the template file, as described above.
            </p>
            <p>
              Note that the chromatic.ly file also allows the use of
              &#8220;lower-octave&#8221; and &#8220;upper-octave&#8221; in the
              template file, see above. This provides the octave number of the
              lowest octave and highest octave displayed for a continuous staff.
            </p>
            <p>&nbsp;</p>
            <h3>To Do List</h3>
            <p>
              This was Dalley&#8217;s to do list. They are listed in roughly the
              order he considered to be most important, with later items in the
              list less precisely ordered than the earlier items.
            </p>
            <ol>
              <li>
                Center time-signature-engraver and rests when staff is not at 0.
              </li>
              <li>
                Investigate non-standard note heads.<br />
                (See note-head-solfa.ly, <a
                  href="http://lilypond.org/doc/v2.18/Documentation/notation/note-heads#shape-note-heads"
                  >Shape note heads</a
                >, <a
                  href="http://lilypond.org/doc/v2.18/Documentation/notation/note-head-styles"
                  >Note head styles</a
                >, and <a
                  href="http://lilypond.org/doc/v2.18/Documentation/notation/the-feta-font"
                  >The Feta Font</a
                >.)
              </li>
              <li>
                Add a LilyPond conversion file which makes clef and key commands
                do nothing, or at least be consistent with alternative
                notations.
              </li>
              <li>Add other notation systems with standard note heads.</li>
              <li>
                Allow a notehead to be either hollow or solid (white or black)
                according to the note&#8217;s pitch, for notations that require
                this.
              </li>
              <li>Add dotted staff lines.</li>
              <li>Allow multiple clefs.</li>
              <li>Allow for non-standard stems.</li>
              <li>Drop octaves which are unused for several measures.</li>
              <li>Make conversion between notations easier.</li>
            </ol>
            <p>
              This is an incomplete list of just a few of the items that Dalley
              completed:
            </p>
            <ol>
              <li>
                Allow arbitrary center point for staves, which allows other
                notations similar to 6-6 Tetragram.
              </li>
              <li>
                Make creation of arbitrary size staves easier. (In progress).
              </li>
              <li>
                check for 8va and 15ma signs, or some type of register signs for
                alternative notations.
              </li>
            </ol>
            <p>
              <em
                >If you would like to help contribute to this effort, please <a
                  href="/home/about-faq-contact-info/#contact">contact us</a
                >. <em
                  >See also: <a
                    href="http://lilypond.org/web/devel/participating/"
                    target="_blank">Participating in LilyPond development</a
                  >
                </em>
              </em>
            </p>
            <p>&nbsp;</p>
            <h2>Patches to LilyPond</h2>
            <p>
              <a
                href="http://git.savannah.gnu.org/gitweb/?p=lilypond.git&a=search&h=HEAD&st=author&s=Kevin+Dalley"
                rel="nofollow"
                >Kevin Dalley&#8217;s patches for alternative notations that
                have been committed to LilyPond</a
              >
            </p>
            <p>
              An &#8220;internal-leger-lines&#8221; patch by Dalley was
              submitted to LilyPond, but was not committed because it still
              required additional work.  It has now been superseded by code
              contributed by Piers Titus that added more comprehensive support
              for customizing ledger lines, see <a
                href="/wiki/software-wiki/lilypond/">LilyPond</a
              >. (For the record: <a
                href="http://code.google.com/p/lilypond/issues/detail?id=1193#makechanges"
                rel="nofollow"
                >Issue 1193: (PATCH) Enhancement: internal leger lines</a
              >, and here is a separate feature request concerning custom ledger
              lines: <a
                href="http://code.google.com/p/lilypond/issues/detail?id=1292"
                rel="nofollow"
                >Issue 1292: Enhancement: twelve-notation support</a
              > and a <a
                href="http://old.nabble.com/Twelve-tone-notation-td29808432.html"
                rel="nofollow">related email exchange</a
              >.)
            </p>
            <p>
              Dalley also worked on <code>shapeLayoutFunction</code> for custom note
              head shapes. A patch for it was never submitted to LilyPond. Here&#8217;s
              an <a
                href="http://www.mail-archive.com/lilypond-user@gnu.org/msg29239.html"
                rel="nofollow">email exchange</a
              > about it on the lilypond-user listserv from April 2007.  It and his
              full modified version of LilyPond can be found <a
                href="https://github.com/drewm1980/lilypond-an/wiki/"
                target="_blank">here</a
              > on GitHub.
            </p>
            <p>
              Since most of Kevin&#8217;s patches are now part of LilyPond, his
              chromatic.ly file and template file will work, except for:
            </p>
            <ol>
              <li>
                The missing &#8220;internal-leger-lines&#8221; patch (see
                above).  It should now be possible to revise Kevin&#8217;s files
                so they will work with LilyPond&#8217;s custom ledger lines
                feature.
              </li>
              <li>
                His <code>shapeLayoutFunction</code> for customizing note head shapes,
                for notations that require this.
              </li>
              <li>
                Additional note head symbols, for notations that require them,
                such as Twinline and TwinNote.
              </li>
            </ol>
            <p>
              Of course, 2 and 3 are only relevant for systems that use custom
              note head shapes.
            </p>
          </div>
        </div>
      </article>
    </div>
  </div>
</MnpLayoutWiki>
