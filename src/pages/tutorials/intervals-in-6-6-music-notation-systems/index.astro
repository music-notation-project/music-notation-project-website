---
import MnpLayout from "../../../layouts/MnpLayout.astro";
import { makeMetaTitle } from "../../../utils/utils";
---

<MnpLayout
  metaTitle={makeMetaTitle(
    "Intervals in 6-6 Music Notation Systems",
    "Tutorials"
  )}
  metaDescription="Interval relationships between notes are clearly and consistently represented in alternative music notation systems with a 6-6 pitch pattern."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="tutorials type-tutorials status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Intervals in 6-6 Music Notation Systems</h1>
        </header>

        <div class="entry-content">
          <p>
            <em
              >Music notation systems with a chromatic staff and a 6-6 pitch
              pattern offer a much more accurate representation of interval
              relationships than traditional notation. The illustrations below
              show harmonic intervals, but what is being said also applies to
              melodic intervals. This tutorial is part of a series that includes <a
                title="Intervals in Traditional Music Notation"
                href="/tutorials/intervals/"
                >Intervals in Traditional Music Notation</a
              > and <a
                title="Reading and Playing Music by Intervals"
                href="/tutorials/reading-playing-music-intervals/"
                >Reading and Playing Music by Intervals</a
              >.
            </em>
          </p>
          <p>
            In notation systems with a <a href="/">chromatic staff</a> and a <a
              href="/tutorials/6-6-and-7-5-pitch-patterns/">6-6 pitch pattern</a
            > each interval has its own unique appearance so that there is always
            a one-to-one correspondence between what one hears and what one sees.
            This eliminates the ambiguity and inconsistency found in traditional
            notation. It allows one to grasp the tonal structure of music, its scales,
            chords, and other patterns, by making these structures explicit rather
            than implicit.
          </p>
          <p>
            We will consider three different alternative notation systems and
            how they represent intervals. Each of these systems takes a
            different approach to the 6-6 pitch pattern, &#8216;encoding&#8217;
            it either in line pattern, notehead color, or notehead shape.
          </p>
          <p>&nbsp;</p>
          <h2 class="divider-heading">
            Intervals in Notation Systems with a 6-6 Line Pattern
          </h2>
          <p>
            Our first example is <a
              href="/systems/6-6-tetragram-by-richard-parncutt/"
              >6-6 Tetragram</a
            > by Richard Parncutt which has a 6-6 line pattern. At first glance it
            appears quite similar to traditional notation since intervals are also
            distinguished by being either being (1) two notes that are both on lines
            or both on spaces, or (2) a combination of a line-note and a space-note.
            The difference is that on a chromatic staff this becomes a means to <em
              >fully</em
            > identify intervals. For example, the difference between major and minor
            seconds is clear rather than just the difference between seconds and
            thirds.
          </p>
          <p>
            Note that this similarity to traditional notation can also be cause
            for confusion when switching between the two. For instance what
            looks like it would be a fifth in traditional notation is a major
            third in Parncutt&#8217;s 6-6 Tetragram. Intervals that look the
            same when moving from one system to the other are actually not the
            same interval. This is the case for any 6-6 notation system where
            the lines are spaced a major second (whole step) apart. So there is
            a positive and negative aspect to this similarity with traditional
            notation. We will look at a couple of other notations that address
            this in different ways.
          </p>
          <p>&nbsp;</p>
          <p>
            <strong
              >Chromatic Intervals in Parncutt&#8217;s Tetragram Notation</strong
            >
          </p>
          <p>
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/Parncutt-Intervals1b2.gif"
              alt="Chromatic Intervals in Parncutt's Tetragram Notation"
              width="670"
              height="572"
            />
          </p>
          <p>
            The next illustration shows how the consistency of a 6-6 line
            pattern makes explicit the differences in diatonic intervals that
            are obscured by traditional notation.
          </p>
          <p>&nbsp;</p>
          <p>
            <strong
              >Diatonic Intervals in Parncutt&#8217;s Tetragram Notation, Key of
              C Major</strong
            >
          </p>
          <p>
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/Parncutt-Intervals2b.gif"
              alt="Diatonic Intervals in Parncutt's Tetragram Notation, Key of C Major"
              width="670"
              height="600"
            />
          </p>
          <p>&nbsp;</p>
          <h2 class="divider-heading">
            Intervals in Systems with a 6-6 Black and White Notehead Pattern
          </h2>
          <p>
            The next example we will consider is <a
              href="/systems/untitled-by-johannes-beyreuther/"
              >Johannes Beyreuther&#8217;s Untitled</a
            > notation, which has a 6-6 pattern in its noteheads. One whole-tone
            scale is represented with white/hollow noteheads, and the other is represented
            with black/solid noteheads (see <a
              href="/tutorials/noteheads-and-pitch/">Noteheads and Pitch</a
            >). Intervals are distinguished by either being (1) two notes that
            are both white or both black notes, or (2) a combination of a black
            note and a white note. This distinction allows one to fully identify
            any given interval since the intervals have a consistent appearance.
            However, one drawback to this approach is that there may be
            confusion from the use of black and white noteheads for pitch rather
            than rhythm.
          </p>
          <p>
            Note that the lines are spaced further apart, a major third to be
            precise. This gives intervals a &#8220;wider&#8221; appearance than
            one is used to in traditional notation, and may seem odd at first.
            This also means that the intervals do not look just like those in
            traditional notation, while actually being different and sounding
            different (as we saw with Parncutt&#8217;s 6-6 Tetragram above).
          </p>
          <p>
            6-6 notations that have their lines spaced a major third apart do
            offer some useful continuity with traditional notation, since the
            lines in traditional notation are also spaced a third apart (either
            a major or minor third). This may help make it easier to transition
            back and forth between the two. For instance, note how major thirds
            span the same distance as the lines, as is the case in traditional
            notation. This continuity is not exact, but in general the spacing
            between intervals and lines will be closer to what one is used to in
            traditional notation.
          </p>
          <p>&nbsp;</p>
          <p>
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/BeyrChrom.gif"
              alt="Chromatic Intervals in Beyreuther's Untitled Notation"
              width="710"
              height="670"
            />
          </p>
          <p>
            The next illustration shows how the consistency of a 6-6 pattern in
            notehead color makes explicit the differences in diatonic intervals
            that are obscured by traditional notation.
          </p>
          <p>
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/BeyrDia.gif"
              alt="Diatonic Intervals in Beyreuther's Untitled Notation, Key of C Major"
              width="710"
              height="700"
            />
          </p>
          <p>&nbsp;</p>
          <h2 class="divider-heading">
            Intervals in Notation Systems with a 6-6 Notehead Shape Pattern
          </h2>
          <p>
            The last example we will consider is <a
              href="/systems/twinline-notation-by-thomas-reed/"
              >Twinline notation</a
            > by Thomas Reed, which has a 6-6 pattern in its notehead shapes. One
            whole-tone scale is represented with oval noteheads, and the other is
            represented with triangular noteheads. Intervals are distinguished by
            either being (1) two notes that are both ovals or both triangles, or
            (2) a combination of a triangle and an oval. Like the other notations
            above, this distinction allows one to fully identify any given interval
            since the intervals have a consistent appearance.
          </p>
          <p>
            The lines are spaced a major third apart as with Beyreuther&#8217;s
            Untitled notation above, but the use of the triangle noteheads
            allows the staff to be vertically &#8220;compressed&#8221; while
            still maintaining even spacing between the centers of the noteheads
            on the vertical pitch axis. Intervals do not seem as
            &#8220;wide&#8221; as in either of the notations above, but the use
            of triangle noteheads may seem odd or confusing at first.
          </p>
          <p>
            The lines are spaced a major third apart but are
            &#8220;physically&#8221; the same distance apart as in traditional
            notation. This provides even more familiar continuity with
            traditional notation as with Beyreuther&#8217;s untitled notation
            above, provided one accepts the unfamiliarity of the different
            notehead shapes.
          </p>
          <p>
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/TLChrom.gif"
              alt="Chromatic Intervals in Twinline Notation by Thomas Reed"
              width="710"
              height="620"
            />
          </p>
          <p>
            The next illustration shows how the consistency of a 6-6 pattern in
            notehead shape makes explicit the differences in diatonic intervals
            that are obscured by traditional notation.
          </p>
          <p>
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/TLDia.gif"
              alt="Diatonic Intervals in Twinline Notation by Thomas Reed, Key of C Major"
              width="710"
              height="600"
            />
          </p>
          <p>
            This concludes this pair of tutorials on intervals. As we have
            shown, traditional notation does not fully and clearly represent
            interval relationships, but builds interval inconsistency into its
            staff. Alternative notations that have a chromatic staff and a 6-6
            pitch pattern do a much better job representing intervals
            consistently, fully, and clearly, providing significant benefits for
            musicians.
          </p>
          <p>
            See also the following PDF files which show scales and chords in
            various 6-6 and 7-5 notations:
          </p>
          <ul>
            <li>
              <a href="/pdf/comparisons/MajorScales.pdf" target="_blank">
                Major Scale Comparison</a
              >
            </li>
            <li>
              <a href="/pdf/comparisons/Triads.pdf" target="_blank">
                Triad Comparison</a
              >
            </li>
            <li>
              <a href="/pdf/comparisons/JazzChords.pdf" target="_blank">
                Jazz Chords Comparison</a
              >
            </li>
          </ul>
        </div>
      </article>

      <div class="divider-section">
        <nav id="nav-single">
          <h3 class="assistive-text">Tutorial navigation</h3>
          <p class="tut-nav-next">
            <a
              href="/tutorials/chromatic-staves-example/"
              rel="prev"
              class="tutorial-nav-link"
              ><span class="meta-nav">&rarr;</span> Next: Chromatic Staves Example</a
            >
          </p>
          <p class="tut-nav-previous">
            <a
              href="/tutorials/reading-playing-music-intervals/"
              rel="next"
              class="tutorial-nav-link"
              ><span class="meta-nav">&larr;</span> Previous: Reading and Playing
              Music by Intervals</a
            >
          </p>
        </nav>
      </div>
    </div>
  </div>
</MnpLayout>
