---
import MnpLayout from "../../../layouts/MnpLayout.astro";
import { makeMetaTitle } from "../../../utils/utils";
---

<MnpLayout
  metaTitle={makeMetaTitle("Numerical Notation Systems", "Tutorials")}
  metaDescription='Historical overview and discussion of various numerical music notation systems, drawing on "Source Book of Proposed Music Notation Reforms" by Gardner Read.'
>
  <div id="primary">
    <div id="content" role="main">
      <article class="tutorials type-tutorials status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Numerical Notation Systems</h1>
        </header>

        <div class="entry-content">
          <p>
            Elsewhere on the Music Notation Project&#8217;s Web site, we focus
            on systems that represent the chromatic scale graphically, with a
            pitch-proportional staff. However, it is instructive to also examine
            systems that represent the chromatic scale numerically and not
            necessarily graphically. In such systems, pitches are represented by
            numerals.
          </p>
          <p>
            One benefit of a numerical approach is that it makes it easier to
            calculate the interval relationships between notes — instead of, or
            in addition to, representing interval relationships graphically on a
            staff. Most of these systems place all the notes/numbers of one
            octave at the same vertical position. This approach allows these
            systems to be vertically very compact. However, this benefit is
            gained by sacrificing the benefits of a pitch-proportional graphical
            representation of notes (<a href="/notationsystems/criteria/#8"
              >#8</a
            > of our <a href="/notationsystems/criteria/"
              >Desirable Criteria&#8230;</a
            >).
          </p>
          <p>
            This tutorial contains two parts: a <a
              href="/tutorials/numerical-notation-systems/#historical"
              >historical background</a
            > written mostly by Dominique Waller (himself an inventor of a numerical
            notation system), and a <a
              href="/tutorials/numerical-notation-systems/#chronology"
              >chronology</a
            > of important innovations, extracted from information in Gardner Read&#8217;s
            <em>Source Book of Proposed Music Notation Reforms</em>.
          </p>
          <p>
            Also see the <a href="/wiki/Nomenclatures_Overview"
              >overview of nomenclatures</a
            > page on the Music Notation Project Wiki, especially <a
              href="/w/images/2/21/Waller_single_digit_symbols.pdf"
              >Dominique Waller&#8217;s document</a
            > on the various <em>single-digit</em> symbols that have been proposed
            as substitutes for <em>double-digit</em> numbers in numerical notation
            systems (i.e. 10 and 11).
          </p>
          <p><a id="historical"></a></p>
          <h2 class="divider-heading">Historical Background</h2>
          <p>(with many thanks to Dominique Waller)</p>
          <p>
            In considering the development of numerical musical notation through
            the centuries, one can see that there has been a general progression
            in the use of numbers by notation inventors as everyone&#8217;s
            understanding of arithmetic itself progressed.
          </p>
          <p>
            Europeans generally used Roman numerals until the mid-16th century,
            when Arabic numerals became widely adopted. Even after the adoption
            of Arabic numerals, the zero (which is so useful for measurement)
            was often ignored, or at least not fully understood as a legitimate
            number. Roman numerals, which are seen on many sundials and clocks
            to this day, were still sufficient for counting and ordering things.
            One can speculate that it took some time for Europeans to become
            fully acclimated to the idea of considering numbers as direct
            representations for measurement, as opposed to names for positions
            in a sequence. Thus the early numerical systems didn&#8217;t measure
            intervallic distances directly, but simply named the successive
            degrees of the diatonic (modal) scale. Since these successive
            degrees are unequally spaced (with some the distance of a semitone
            apart and others a whole tone), the concept of using pitch names for
            direct measurement of musical intervals didn&#8217;t apply.
          </p>
          <p>
            Here is an early example written in a Spanish tablature for the
            harp:<br />
            &nbsp;<br />
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/Cabezon-Spanish-Harp-Tablature.png"
              alt="Example of early Spanish tablature for the harp"
              width="745"
              height="204"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            This figure comes from Hernando de Cabezón, <em
              >Obras para tecla, arpa y vihuela</em
            >, Madrid, 1578<strong>.</strong><strong> </strong>The piece is
            written in the idiom of Renaissance polyphony, here with five
            different voices. Lines represent individual voices, numbers denote
            the seven degrees of the diatonic scale, and various diacritical
            signs (dots, commas) indicate different octaves.
          </p>
          <p>
            The harmony was fundamentally unaffected by this coding, as it is
            itself based on those seven steps, beginning with the name of the
            intervals themselves: unison, second, third, fourth etc. Those
            intervals are variable, as they can be minor, major, augmented,
            diminished, etc., depending on the scale itself and the alterations
            (simple or, later, double) assigned to the notes. Therefore, this
            coding was just the numerical translation of a 7-tone scale and of
            the harmony that goes with it.
          </p>
          <p>
            The following figure (from <em
              >Traité de la musique théorique et pratique contenant les
              préceptes de la composition</em
            >, 1639, new edition 1646, p. 77) shows what is perhaps the first
            numerical staff in history, designed by Jesuit father Antoine
            Parran:<br />
            &nbsp;<br />
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/Numerical-staff-Antoine-Parran.png"
              alt="Numerical musical staff by Antoine Parran"
              width="745"
              height="154"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            On this staff, the subject (theme) of the composition is given in
            the form of diatonic numbers, in place of noteheads.
          </p>
          <p>
            Here is an example of Jean-Jacques Rousseau’s numerical notation
            (from <em
              ><a
                href="http://en.wikipedia.org/wiki/Encyclop%C3%A9die"
                target="_blank"
                >Dictionnaire de l’Encyclopédie de d’Alembert et Diderot</a
              >
            </em> (published 1751–72), Volume VII, pl. IV, fig. 17):<br />
            &nbsp;<br />
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/Rousseau-numerical-music-notation.png"
              alt="Example of Jean-Jacques Rousseau's numerical music notation"
              width="745"
              height="82"
            />
          </p>
          <p>&nbsp;</p>
          <p>The dots indicate a shift to an upper or lower octave.</p>
          <p>
            As the result of the general acceptance of the positional system of
            numerals, that is, the place-value notation using Arabic figures and
            including the zero, it became possible to easily subdivide numbers
            on a decimal basis, a fact that allowed numbers to develop the new
            function of measuring things with precision. In the 16th century,
            the mathematician Simon Stevin advocated for a duodecimal (that is,
            base-12) number system. The Western world did not follow him.
            However, in addition, he advocated for fractional division after the
            decimal point, and this time he opened the way for the uses of
            decimal divisions in unit systems. Later on, new standards of
            rational measurement were invented that took advantage of these
            advances, resulting in the adoption of the metric system by the
            French Convention in 1792.
          </p>
          <p>
            In the course of the 18th century, equal temperament began to settle
            down in the mind of musicians and music theorists. As a consequence,
            notation systems with the 1-12 series then flourished, but this
            nomenclature could still be understood as ordinal numbers ordering
            and naming the notes of the series. With the next stage, the 0-11
            series, it became self-evident that numbers endorsed the new
            function of measuring pitches and intervals with the prony (twelfth
            part of the octave) as the unit of measure, exactly as a tape
            measure does.
          </p>
          <p>
            As early as 1832, Baron Blein, a French engineer, realized that this
            could provide the basis for a new language for harmony, and he began
            to develop chord analysis in this pure arithmetical coding together
            with a chromatic nomenclature of 12 syllables. But he didn’t have
            the idea of using those same numbers in his 7-line staff system,
            which was plainly chromatic. (That’s why he is not registered in
            Gardner Read’s book as a numerical system inventor; see p. 17.) The
            following figure by baron Blein (from <em
              >Principes de mélodie et d’harmonie</em
            >, Paris, 1838, p. 93, French National Library) shows a two-octave
            chromatic scale on a chromatic staff. The numbers underneath measure
            intervals in terms of semitones.<br />
            &nbsp;<br />
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/Baron-Blein-music-notation.png"
              alt="Baron Blein music notation system"
              width="745"
              height="212"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            In the late 19th century, inventors such as the Austro-Hungarian Leo
            Kuncze and the Mexican Julián Carrillo (see <a
              href="/tutorials/numerical-notation-systems/#chronology"
              >chronology</a
            > below) introduced numerical notation systems based on the chromatic
            scale. Kuncze&#8217;s system used the series 1-12, whereas Carrillo&#8217;s
            used 0-11. Here are the four main octaves in Kuncze&#8217;s notation,
            written in between the treble and bass staves of traditional notation:<a
              id="kuncze"></a><br />
            &nbsp;<br />
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/Kuncze-music-notation.png"
              alt="Leo Kuncze music notation system"
              width="745"
              height="193"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            Both Kuncze and Carrillo understood that if the horizontal bar is to
            symbolize the octave, then it is useful to have it <em>above</em> ciphers
            for low octaves and <em>under</em> ciphers for high octaves. Whereas
            other inventors had followed a logical impulse to place a symbol above
            a number to raise its octave, and below it to lower its octave, Kuncze
            and Carrillo recognized that the opposite scheme has the advantage of
            resulting in a vertical pitch axis of a sort: higher octaves appear higher
            on the page than lower octaves. The resolution of this pitch axis is
            only one octave, whereas the pitch-proportional chromatic notations described
            elsewhere on this site have a pitch resolution of one semitone. (In traditional
            notation, the resolution of the pitch axis is irregular, being either
            a semitone or a whole tone and, in the presence of accidentals, sometimes
            becoming an augmented second, etc.)
          </p>
          <p>
            Here are eight octaves in Carrillo&#8217;s system (from <em
              >Nueva escritura musical</em
            >, 1895), placed below traditional notation as a reference:<a
              id="carrillo"></a><br />
            &nbsp;<br />
            <img
              loading="lazy"
              src="/wp-content/uploads/2013/03/Carrillo-music-notation.png"
              alt="Julian Carrillo music notation system"
              width="745"
              height="322"
            />
          </p>
          <p>&nbsp;</p>
          <p>
            For maximum compactness, Carrillo places numerals on, under, over
            and between line or bars.
          </p>
          <p>&nbsp;</p>
          <p>
            Many other inventors have introduced alternative symbols for the
            numbers 10 and 11, to facilitate duodecimal (base-12) calculations
            and to avoid ambiguity when notes are placed close together. (For
            example, is 11 a single note or two instances of pitch 1 in close
            rhythmic proximity?) See the discussion of such symbols on the <a
              href="/w/images/2/21/Waller_single_digit_symbols.pdf"
              >Music Notation Project&#8217;s Wiki</a
            >.
          </p>
          <p>
            Today, a diatonic numerical notation using the numbers 1-7 is
            widespread in China, where the system is known as jiǎnpǔ, literally
            &#8220;simplified notation&#8221; (see the <a
              href="http://en.wikipedia.org/wiki/Numbered_musical_notation"
              >jiǎnpǔ Wikipedia article</a
            >). The digit 0 represents a rest.
          </p>
          <p>
            The seven notes are apparently not necessarily identical to those of
            the European major scale, according to the <a
              href="http://en.wikipedia.org/wiki/Gongche_notation"
              >Gongche notation article</a
            > on Wikipedia. Much traditional Chinese music is based on pentatonic
            scales, and yet the Chinese have apparently adapted to a 7-degree notation.
            One might be encouraged by this fact to speculate that it should therefore
            be similarly possible for a 12-degree notation to take hold in the West,
            even though most Western music is firmly grounded in the diatonic scale.
          </p>
          <p><a id="chronology"></a></p>
          <h2 class="divider-heading">
            Chronology of Innovations in Numerical Notation
          </h2>
          <p>
            Gardner Read&#8217;s <em
              >Source Book of Proposed Music Notation Reforms</em
            > (Greenwood Press, 1987) describes many alternative notation systems.
            Chapter 5, &#8220;Numerical Systems,&#8221; describes 44 music notation
            proposals in which pitches are represented by numbers (at least supplementarily,
            if not exclusively). The following list summarizes that chapter, focusing
            only on the first historical appearance of specific features.<a
              id="ftnref1"
              href="/tutorials/numerical-notation-systems/#ftn1">[1]</a
            >
          </p>
          <h3>Before 1877</h3>
          <p>
            1560: the earliest numerical system appears, by Pierre Davantès. It
            and all other systems until the year 1877 use the numbers 1-7 to
            indicate one octave&#8217;s worth of modal or diatonic pitches. The
            intervening chromatic pitches are either ignored or represented by
            various alterations of these numbers. Generally ut or C is 1. (In
            the early modal systems, ut is G, represented by 1.)
          </p>
          <h3>1877 – 1900</h3>
          <p>
            1877: the first chromatic numerical system appears, by H. Hohmann.
            It uses 1-9 for C through Ab, a sort of cross symbol for A, 0 for
            Bb, and B for B (the English B, not the German B). Other
            diatonic-based numerical systems (using the numbers 1-7) continue to
            appear in the 19th and 20th centuries.
          </p>
          <p>
            1877: Leo Kuncze uses 1-9 for C-Ab, 0 for A, and upside-down 1 and 2
            for Bb and B respectively. (See the <a
              href="/tutorials/numerical-notation-systems/#kuncze"
              >image above</a
            >.)
          </p>
          <p>
            1892: Hans Schmitt uses 1-9 for C-Ab, a small circle with
            &#8220;horns&#8221; for A, U for Bb, and S for B.
          </p>
          <p>
            1895: the first zero-based system appears, by <a
              href="http://www.sonido13.com/">Julián Carrillo</a
            >, with 0-9 for C-A, followed by 10 and 11 for Bb and B. (His
            notation system can be seen <a
              href="/tutorials/numerical-notation-systems/#carrillo">above</a
            > and is documented at the official Web site of the Julián Carrillo Estate:
            <a href="http://www.sonido13.com/nuevaescrituramusical1.html">1</a>
            <a href="http://www.sonido13.com/nuevaescrituramusical2.html">2</a>
            <a href="http://www.sonido13.com/nuevaescrituramusical3.html">3</a
            >.)
          </p>
          <p>
            1897: Charles Guilford uses 1-9 for C-Ab, followed by 10, 11, 12 for
            A, Bb, B.
          </p>
          <h3>1900 – 1987</h3>
          <p>
            [not mentioned in Read&#8217;s book] 1946 or earlier: Carl-Gustaf
            Rosengren&#8217;s system appears to place notes at three vertical
            positions per octave.
          </p>
          <p>
            1966: John Robbins introduces a highly idiosyncratic system in which
            the notes use a different number sequence for left versus right hand
            of the piano, and there are only 10 unique digits for 12 unique
            pitches.
          </p>
          <p>
            1970: Christopher Crocker&#8217;s system uses 1-12 for do through
            ti.
          </p>
          <p>
            1973: Yael Bukspan proposes a system that uses binary numbers for
            the 12 chromatic pitches (though not the series of binary numbers
            one might expect).
          </p>
          <p>
            1983: Robert Stuckey introduces the first system that uses numerical
            noteheads on a pitch-proportional chromatic staff. It uses 0-9 for
            C-A, followed by X and N for Bb and B. The other chromatic numerical
            systems to this point typically placed all the numbers of one octave
            at the same vertical position. <a
              href="/systems/untitled-by-robert-stuckey/"
              >Stuckey&#8217;s system</a
            > is presented on our site.
          </p>
          <h3>1987 – Present</h3>
          <p>
            The following systems were proposed after Read&#8217;s book was
            published in 1987. There are likely others that have escaped our
            attention.
          </p>
          <p>
            <a
              href="http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&Sect2=HITOFF&d=PALL&p=1&u=/netahtml/PTO/srchnum.htm&r=1&f=G&l=50&s1=7,674,965.PN.&OS=PN/7,674,965&RS=PN/7,674,965"
              >FinKeys Notation System</a
            > by Victor Mataele uses the numbers 1-9 followed by the letters X, Y,
            and Z (C is 1 and B is Z).
          </p>
          <p>
            <a href="/wiki/Hamburg_Music_Notation">Hamburg Music Notation</a> by
            Robert Elisabeth Key, uses 0-9 followed by the letters A and B (C is
            1, A is A, Bb is B, and B is 0). Note that &#8220;Robert Elisabeth Key&#8221;
            is a pseudonym.
          </p>
          <p>
            <a href="/systems/numbered-notes-numbers-only-by-jason-maccoy/"
              >Numbered Notes</a
            > by Jason MacCoy uses 1-12 for C-B, and uses a pitch-proportional chromatic
            staff with lines spaced a minor third apart.
          </p>
          <p>&nbsp;</p>
          <div class="footnotes divider-section">
            <p>
              <a id="ftn1" href="/tutorials/numerical-notation-systems/#ftnref1"
                >[1]</a
              > Read&#8217;s chapter includes many systems not mentioned here. This
              tutorial focuses only on the first appearance of specific features,
              according to the chronology in his survey. It is likely that some numerical
              systems introduced before 1987 escaped his attention. Also, it is possible
              that there are some errors in the summary above; there are quite a
              few errors in Read&#8217;s book, which is nonetheless the most thorough
              and detailed collection of its type.
            </p>
          </div>
        </div>
      </article>

      <div class="divider-section">
        <nav id="nav-single">
          <h3 class="assistive-text">Tutorial navigation</h3>
          <p class="tut-nav-next"></p>
          <p class="tut-nav-previous">
            <a
              href="/tutorials/enharmonic-equivalents/"
              rel="next"
              class="tutorial-nav-link"
              ><span class="meta-nav">&larr;</span> Previous: Enharmonic Equivalents</a
            >
          </p>
        </nav>
      </div>
    </div>
  </div>
</MnpLayout>
