---
import MnpLayout from "../../../layouts/MnpLayout.astro";
import { makeMetaTitle } from "../../../utils/utils";
---

<MnpLayout
  metaTitle={makeMetaTitle("Noteheads and Pitch", "Tutorials")}
  metaDescription="Some alternative music notation systems use different noteheads to help indicate pitch instead of rhythm. This approach has advantages and disadvantages."
>
  <div id="primary">
    <div id="content" role="main">
      <article class="tutorials type-tutorials status-publish hentry">
        <header class="entry-header">
          <h1 class="entry-title">Noteheads and Pitch</h1>
        </header>

        <div class="entry-content">
          <p>
            Some alternative music notations use white or black (i.e. hollow and
            solid) noteheads to help indicate a note&#8217;s pitch, instead of
            its duration as in traditional notation. (See <a href="/systems/"
              >Music Notation Systems: Gallery</a
            > sorted by 7-5 or 6-6 patterns for examples.) This may seem like an
            odd and unnecessary departure from traditional notation, but there is
            a good rationale behind it.
          </p>
          <p>
            Arguably the most striking feature of any given note is whether it
            is black or white. Glancing quickly at a piece of traditional sheet
            music, or holding it at a distance, it is easier to notice whether a
            notehead is black or white than to discern which staff line or space
            it occupies. However, traditional notation uses this property merely
            to distinguish between half notes and quarter notes (minims and
            crotchets):
          </p>
          <div>
            <img
              loading="lazy"
              class="alignnone"
              style="border: 0px none;"
              alt="Duration symbols in traditional music notation"
              src="/wp-content/uploads/2013/03/tradnoteheads1.gif"
              width="440"
              height="75"
            />
          </div>
          <p>
            The traditional rhythmic symbols like stems and flags make it
            possible to tell a note&#8217;s duration without needing to refer to
            the notehead, except for half notes and quarter notes:
          </p>
          <div>
            <img
              loading="lazy"
              alt="Diagram of how duration is mostly not indicated with note heads"
              src="/wp-content/uploads/2013/03/tradnoteheads23.gif"
              width="440"
              height="145"
            />
          </div>
          <p>
            Some notation designers argue that traditional notation makes poor
            use of the distinction between white and black notes, and that it
            would be better employed to help indicate a note&#8217;s pitch
            &#8212; since it is the feature that most clearly distinguishes one
            note from another. These designers typically use black and white
            noteheads to represent or accentuate either a <a
              href="/tutorials/6-6-and-7-5-pitch-patterns/"
              >6-6 or 7-5 pitch pattern</a
            >. But if they also want to retain the other basic features of
            traditional rhythmic notation, they must devise a new way to
            distinguish half notes from quarter notes.
          </p>
          <p>Some approaches include:</p>
          <ul>
            <li>
              Using different notehead shapes (Busoni&#8217;s Organic Notation
              used rectangles for whole and half notes)
            </li>
            <li>
              Using double stems to indicate half notes (Rich Reed&#8217;s <a
                href="/systems/da-notation-by-rich-reed/">DA Notation</a
              >, John Keller&#8217;s <a
                href="/systems/express-stave-by-john-keller/">Express Stave</a
              >, Paul Morris&#8217; <a href="http://twinnote.org"
                >TwinNote,
              </a>Joe Austin&#8217;s <a
                href="/system/chromatonnetz-by-joe-austin/">ChromaTonnetz</a
              >)
            </li>
            <li>
              Using stems with new kinds of flags (Lindgren&#8217;s <a
                href="http://nydana.se"
                target="_blank">Nydana Notation</a
              >, Thomas Reed&#8217;s <a
                href="/systems/groups/major-3rd-2-lines-compact/">Twinline</a
              >)
            </li>
            <li>
              Breaking more fully with traditional notation by using a
              proportional rhythmic notation (Jean de Buur&#8217;s <a
                href="http://http://musicnotation.org/systems/klavar-mirck-version-by-jean-de-buur/"
                >Mirck Version</a
              > of Klavar; Johannes Beyreuther&#8217;s <a
                href="/systems/chromatic-6-6-notation-by-johannes-beyreuther/"
                >Chromatic 6-6 Notation</a
              >)
            </li>
            <li>
              Or other more complex rhythmic schemes (Skapski&#8217;s <a
                href="/systems/panot-notation-by-george-skapski/">Panot</a
              >).
            </li>
          </ul>
          <p>
            A potential drawback to using black and white noteheads for pitch is
            that it might make it somewhat more difficult to transition from
            traditional notation (or to switch back and forth
            &#8220;bilingually&#8221;), at least when compared to alternative
            notation systems that maintain full consistency with traditional
            rhythmic notation.
          </p>
          <p>
            One way to address this would be to have two versions of a given
            alternative system, one that retains traditional rhythmic notation
            and another that goes further and introduces its own alternative
            rhythmic notation. While this would not work in every case, it could
            provide an easier and lower-risk way for those trained in
            traditional notation to try out at least some of the alternative
            notation systems that use black and white noteheads for pitch.
          </p>
        </div>
      </article>

      <div class="divider-section">
        <nav id="nav-single">
          <h3 class="assistive-text">Tutorial navigation</h3>
          <p class="tut-nav-next">
            <a href="/tutorials/intervals/" rel="prev" class="tutorial-nav-link"
              ><span class="meta-nav">&rarr;</span> Next: Intervals in Traditional
              Music Notation</a
            >
          </p>
          <p class="tut-nav-previous">
            <a
              href="/tutorials/6-6-and-7-5-pitch-patterns/"
              rel="next"
              class="tutorial-nav-link"
              ><span class="meta-nav">&larr;</span> Previous: 6-6 and 7-5 Pitch Patterns</a
            >
          </p>
        </nav>
      </div>
    </div>
  </div>
</MnpLayout>
