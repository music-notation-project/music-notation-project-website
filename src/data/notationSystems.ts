export const notationSystemSlugsDefaultOrder = [
  "untitled-by-heinrich-richter",
  "untitled-by-grace-frix",
  "c-symmetrical-semitone-notation",
  "notation-godjevatz-by-velizar-godjevatz",
  //
  "a-b-chromatic-notation-by-albert-brennink",
  "6-6-tetragram-by-richard-parncutt",
  "6-6-trigram-notation-by-richard-parncutt",
  //
  "untitled-by-klaus-lieber",
  "notation-for-the-system-of-equal-tones-by-gustave-decher",
  "untitled-by-franz-grassl",
  "ambros-system-by-august-ambros",
  "douzave-system-by-john-leon-acheson",
  "notagraph-by-constance-virtue",
  "note-for-note-by-walter-h-thelwall",
  "proportional-chromatic-musical-notation-by-henri-carcelle",
  //
  "isomorph-notation-by-tadeusz-wojcik",
  "da-notation-by-rich-reed",
  //
  "numbered-notes-notes-only-by-jason-maccoy",
  "numbered-notes-numbers-only-by-jason-maccoy",
  "lautus-notation-by-bruce-allen-dickey",
  "hass-notation-3-line-version",
  "trilinear-notation-jose-sotorrio",
  //
  "chromatic-6-6-notation-by-johannes-beyreuther",
  "6-6-klavar-by-cornelis-pot",
  "untitled-by-arnold-schoenberg",
  //
  "panot-notation-by-george-skapski",
  "clairnote-sn-paul-morris",
  "untitled-by-johannes-beyreuther",
  "clairnote-dn-by-paul-morris",
  "chromatic-lyre-notation-by-jan-braunstein",
  "axlomafi-jai-park",
  //
  "chromatic-twinline-by-leo-de-vries",
  "diatonic-twinline-by-leo-de-vries",
  "twinline-notation-by-thomas-reed",
  "black-triangle-twinline-by-doug-keislar",
  "black-oval-twinline-by-paul-morris",
  "bilinear-notation-by-jose-a-sotorrio",
  "twinnote-by-paul-morris",
  "twinnote-td-by-paul-morris",
  "chromatic-nydana-by-dan-lindgren",
  //
  "muto-notation-by-muto-foundation",
  "thumline-notation-by-jim-plamondon",
  "express-stave-by-john-keller",
  "express-stave-6-6-jazz-font-by-john-keller",
  "express-stave-original-version-by-john-keller",
  "express-stave-tricolor-version-john-keller",
  "untitled-by-robert-stuckey",
  "untitled-by-nicolai-dolmatov",
  //
  "klavar-mirck-version-by-jean-de-buur",
  "klavar-dekker-version-by-antoon-dekker",
  "chromatonnetz-by-joe-austin",
  "keyboard-7-5-trigram-notation-by-richard-parncutt",
  "avique-notation-by-anne-and-bill-collins",
  "0-5-7-notation-by-richard-parncutt",
] as const;

export type NotationSystemSlug = typeof notationSystemSlugsDefaultOrder[number];

export function isNotationSystemSlug(str: string): str is NotationSystemSlug {
  return notationSystemSlugsDefaultOrder.includes(str as NotationSystemSlug);
}

export interface NotationSystem {
  slug: NotationSystemSlug;
  name: string;
  author: string;
  year: number;
  chromaticScaleImagePath: string;
}

export const makeNotationSystemPath = (slug: NotationSystemSlug) =>
  `/system/${slug}/`;

export const makeNotationSystemTitle = (system: NotationSystem) =>
  `${system.name} by ${system.author}`;

// Sort these alphabetically by slug/key.
export const notationSystemsData: Record<NotationSystemSlug, NotationSystem> = {
  "0-5-7-notation-by-richard-parncutt": {
    slug: "0-5-7-notation-by-richard-parncutt",
    name: "0-5-7 Notation",
    author: "Richard Parncutt",
    year: 1984,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/0-5-7-notation-richard-parncutt.png",
  },
  "6-6-klavar-by-cornelis-pot": {
    slug: "6-6-klavar-by-cornelis-pot",
    name: "6-6 Klavar",
    author: "Cornelis Pot",
    year: 1972,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/6-6-klavar-cornelis-pot-horizontal.png",
  },
  "6-6-tetragram-by-richard-parncutt": {
    slug: "6-6-tetragram-by-richard-parncutt",
    name: "6-6 Tetragram",
    author: "Richard Parncutt",
    year: 1996,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/6-6-tetragram-richard-parncutt.png",
  },
  "6-6-trigram-notation-by-richard-parncutt": {
    slug: "6-6-trigram-notation-by-richard-parncutt",
    name: "6-6 Trigram Notation",
    author: "Richard Parncutt",
    year: 1990,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/6-6-trigram-richard-parncutt.png",
  },
  "a-b-chromatic-notation-by-albert-brennink": {
    slug: "a-b-chromatic-notation-by-albert-brennink",
    name: "A-B Chromatic Notation",
    author: "Albert Brennink",
    year: 1976,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/a-b-chromatic-albert-brennink1.png",
  },
  "ambros-system-by-august-ambros": {
    slug: "ambros-system-by-august-ambros",
    name: "Ambros System",
    author: "August Ambros",
    year: 1883,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/ambros-system-august-ambros.png",
  },
  "avique-notation-by-anne-and-bill-collins": {
    slug: "avique-notation-by-anne-and-bill-collins",
    name: "Avique Notation",
    author: "Anne and Bill Collins",
    year: 1974,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/avique-anne-bill-collins.png",
  },
  "axlomafi-jai-park": {
    slug: "axlomafi-jai-park",
    name: "AxLoMaFi",
    author: "Jai Park",
    year: 2014,
    chromaticScaleImagePath:
      "/wp-content/uploads/2015/01/trine-line-by-jai-park.png",
  },
  "bilinear-notation-by-jose-a-sotorrio": {
    slug: "bilinear-notation-by-jose-a-sotorrio",
    name: "Bilinear Notation",
    author: "José A. Sotorrío",
    year: 1997,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/bilinear-jose-sotorrio.png",
  },
  "black-oval-twinline-by-paul-morris": {
    slug: "black-oval-twinline-by-paul-morris",
    name: "Black-Oval Twinline",
    author: "Paul Morris",
    year: 2006,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/twinline-black-oval-paul-morris.png",
  },
  "black-triangle-twinline-by-doug-keislar": {
    slug: "black-triangle-twinline-by-doug-keislar",
    name: "Black Triangle Twinline",
    author: "Doug Keislar",
    year: 2006,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/twinline-black-triangle-doug-keislar.png",
  },
  "c-symmetrical-semitone-notation": {
    slug: "c-symmetrical-semitone-notation",
    name: "C-Symmetrical Semitone Notation",
    author: "Ronald Sadlier",
    year: 1991,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/c-symmetrical-semitone-ronald-sadlier.png",
  },
  "chromatic-6-6-notation-by-johannes-beyreuther": {
    slug: "chromatic-6-6-notation-by-johannes-beyreuther",
    name: "Chromatic 6-6 Notation",
    author: "Johannes Beyreuther",
    year: 1985,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/chromatic-66-johannes-beyreuther.png",
  },
  "chromatic-lyre-notation-by-jan-braunstein": {
    slug: "chromatic-lyre-notation-by-jan-braunstein",
    name: "Chromatic Lyre Notation",
    author: "Jan Braunstein",
    year: 2009,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/chromatic-lyre-notation-jan-braunstein.png",
  },
  "chromatic-nydana-by-dan-lindgren": {
    slug: "chromatic-nydana-by-dan-lindgren",
    name: "Chromatic Nydana",
    author: "Dan Lindgren",
    year: 2011,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/nydana-chromatic-dan-lindren.png",
  },
  "chromatic-twinline-by-leo-de-vries": {
    slug: "chromatic-twinline-by-leo-de-vries",
    name: "Chromatic Twinline",
    author: "Leo de Vries",
    year: 1986,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/twinline-chromatic-leo-de-vries.png",
  },
  "chromatonnetz-by-joe-austin": {
    slug: "chromatonnetz-by-joe-austin",
    name: "ChromaTonnetz",
    author: "Joe Austin",
    year: 2010,
    chromaticScaleImagePath:
      "/wp-content/uploads/2014/02/tonnetz-based-4x3-joe-austin.png",
  },
  "clairnote-dn-by-paul-morris": {
    slug: "clairnote-dn-by-paul-morris",
    name: "Clairnote DN",
    author: "Paul Morris",
    year: 2006,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/clairnote-dn-paul-morris.png",
  },
  "clairnote-sn-paul-morris": {
    slug: "clairnote-sn-paul-morris",
    name: "Clairnote SN",
    author: "Paul Morris",
    year: 2018,
    chromaticScaleImagePath:
      "/wp-content/uploads/2018/12/clairnote-sn-paul-morris.png",
  },
  "da-notation-by-rich-reed": {
    slug: "da-notation-by-rich-reed",
    name: "DA Notation",
    author: "Rich Reed",
    year: 1986,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/da-notation-rich-reed.png",
  },
  "diatonic-twinline-by-leo-de-vries": {
    slug: "diatonic-twinline-by-leo-de-vries",
    name: "Diatonic Twinline",
    author: "Leo de Vries",
    year: 1986,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/twinline-diatonic-leo-de-vries.png",
  },
  "douzave-system-by-john-leon-acheson": {
    slug: "douzave-system-by-john-leon-acheson",
    name: "Douzave System",
    author: "John Leon Acheson",
    year: 1936,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/douzave-john-acheson.png",
  },
  "express-stave-6-6-jazz-font-by-john-keller": {
    slug: "express-stave-6-6-jazz-font-by-john-keller",
    name: "Express Stave, 6-6 Jazz Font",
    author: "John Keller",
    year: 2009,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/express-stave-reverse-color-john-keller1.png",
  },
  "express-stave-by-john-keller": {
    slug: "express-stave-by-john-keller",
    name: "Express Stave: Pianoforte Notation",
    author: "John Keller",
    year: 2010,
    chromaticScaleImagePath:
      "/wp-content/uploads/2021/03/express-stave-pianoforte-notation.png",
  },
  "express-stave-original-version-by-john-keller": {
    slug: "express-stave-original-version-by-john-keller",
    name: "Express Stave, Original Version",
    author: "John Keller",
    year: 2005,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/express-stave-john-keller.png",
  },
  "express-stave-tricolor-version-john-keller": {
    slug: "express-stave-tricolor-version-john-keller",
    name: "Express Stave, Tricolor Version",
    author: "John Keller",
    year: 2010,
    chromaticScaleImagePath:
      "/wp-content/uploads/2015/08/express-stave-tri-color-john-keller.png",
  },
  "hass-notation-3-line-version": {
    slug: "hass-notation-3-line-version",
    name: "Hass Notation, 3-Line Version",
    author: "Peter Hass",
    year: 2014,
    chromaticScaleImagePath:
      "/wp-content/uploads/2015/02/hass-notation-3-line-version-peter-hass.png",
  },
  "isomorph-notation-by-tadeusz-wojcik": {
    slug: "isomorph-notation-by-tadeusz-wojcik",
    name: "Isomorph Notation",
    author: "Tadeusz Wójcik",
    year: 1952,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/isomorph-tadeusz-wojcik.png",
  },
  "keyboard-7-5-trigram-notation-by-richard-parncutt": {
    slug: "keyboard-7-5-trigram-notation-by-richard-parncutt",
    name: "Keyboard (or 7-5) Trigram Notation",
    author: "Richard Parncutt",
    year: 1989,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/7-5-trigram-richard-parncutt.png",
  },
  "klavar-dekker-version-by-antoon-dekker": {
    slug: "klavar-dekker-version-by-antoon-dekker",
    name: "Klavar, Dekker Version",
    author: "Antoon Dekker",
    year: 2015,
    chromaticScaleImagePath:
      "/wp-content/uploads/2015/08/klavar-dekker-antoon-dekker.png",
  },
  "klavar-mirck-version-by-jean-de-buur": {
    slug: "klavar-mirck-version-by-jean-de-buur",
    name: "Klavar, Mirck Version",
    author: "Jean de Buur",
    year: 2006,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/klavar-mirck-jean-de-buur.png",
  },
  "lautus-notation-by-bruce-allen-dickey": {
    slug: "lautus-notation-by-bruce-allen-dickey",
    name: "Lautus Notation",
    author: "Bruce Allen Dickey",
    year: 2011,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/lautus-bruce-dickey.png",
  },
  "muto-notation-by-muto-foundation": {
    slug: "muto-notation-by-muto-foundation",
    name: "MUTO Notation",
    author: "MUTO Music Method Foundation",
    year: 1995,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/muto-muto-music-method-foundation.png",
  },
  "notagraph-by-constance-virtue": {
    slug: "notagraph-by-constance-virtue",
    name: "Notagraph",
    author: "Constance Virtue",
    year: 1933,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/notagraph-constance-virtue.png",
  },
  "notation-for-the-system-of-equal-tones-by-gustave-decher": {
    slug: "notation-for-the-system-of-equal-tones-by-gustave-decher",
    name: "Notation for the System of Equal Tones",
    author: "Gustave Decher",
    year: 1877,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/system-of-equal-tones-gustave-decher.png",
  },
  "notation-godjevatz-by-velizar-godjevatz": {
    slug: "notation-godjevatz-by-velizar-godjevatz",
    name: "Notation Godjevatz",
    author: "Velizar Godjevatz",
    year: 1948,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/notation-godjevatz-velizar-godjevatz.png",
  },
  "note-for-note-by-walter-h-thelwall": {
    slug: "note-for-note-by-walter-h-thelwall",
    name: "Note for Note",
    author: "Walter H. Thelwall",
    year: 1897,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/note-for-note-walter-thelwall.png",
  },
  "numbered-notes-notes-only-by-jason-maccoy": {
    slug: "numbered-notes-notes-only-by-jason-maccoy",
    name: "Numbered Notes, Notes-Only Version",
    author: "Jason MacCoy",
    year: 2009,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/numbered-notes-notes-jason-maccoy.png",
  },
  "numbered-notes-numbers-only-by-jason-maccoy": {
    slug: "numbered-notes-numbers-only-by-jason-maccoy",
    name: "Numbered Notes, Numbers-Only Version",
    author: "Jason MacCoy",
    year: 2009,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/numbered-notes-numbers-jason-maccoy.png",
  },
  "panot-notation-by-george-skapski": {
    slug: "panot-notation-by-george-skapski",
    name: "Panot Notation",
    author: "George Skapski",
    year: 1956,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/panot-george-skapski.png",
  },
  "proportional-chromatic-musical-notation-by-henri-carcelle": {
    slug: "proportional-chromatic-musical-notation-by-henri-carcelle",
    name: "Proportional Chromatic Musical Notation",
    author: "Henri Carcelle",
    year: 1977,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/proportional-chromatic-henri-carcelle.png",
  },
  "thumline-notation-by-jim-plamondon": {
    slug: "thumline-notation-by-jim-plamondon",
    name: "Thumline",
    author: "Jim Plamondon",
    year: 2005,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/thumline-jim-plamondon.png",
  },
  "trilinear-notation-jose-sotorrio": {
    slug: "trilinear-notation-jose-sotorrio",
    name: "Trilinear Notation",
    author: "José A. Sotorrío",
    year: 2017,
    chromaticScaleImagePath:
      "/wp-content/uploads/2017/02/trilinear-jose-a-sotorrio-wpcf_500x110.png",
  },
  "twinline-notation-by-thomas-reed": {
    slug: "twinline-notation-by-thomas-reed",
    name: "Twinline Notation",
    author: "Thomas Reed",
    year: 1986,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/twinline-tom-reed.png",
  },
  "twinnote-by-paul-morris": {
    slug: "twinnote-by-paul-morris",
    name: "TwinNote",
    author: "Paul Morris",
    year: 2009,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/twinnote-paul-morris.png",
  },
  "twinnote-td-by-paul-morris": {
    slug: "twinnote-td-by-paul-morris",
    name: "TwinNote TD",
    author: "Paul Morris",
    year: 2009,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/twinnote-td-paul-morris.png",
  },
  "untitled-by-arnold-schoenberg": {
    slug: "untitled-by-arnold-schoenberg",
    name: "Untitled",
    author: "Arnold Schoenberg",
    year: 1924,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/untitled-arnold-schoenberg.png",
  },
  "untitled-by-franz-grassl": {
    slug: "untitled-by-franz-grassl",
    name: "Untitled",
    author: "Franz Grassl",
    year: 1983,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/untitled-franz-grassl.png",
  },
  "untitled-by-grace-frix": {
    slug: "untitled-by-grace-frix",
    name: "Untitled",
    author: "Grace Frix",
    year: 1992,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/untitled-grace-frix.png",
  },
  "untitled-by-heinrich-richter": {
    slug: "untitled-by-heinrich-richter",
    name: "Untitled",
    author: "Heinrich Richter",
    year: 1847,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/5-line-chromatic-staff-heinrich-richter.png",
  },
  "untitled-by-johannes-beyreuther": {
    slug: "untitled-by-johannes-beyreuther",
    name: "Untitled",
    author: "Johannes Beyreuther",
    year: 1959,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/untitled-johannes-beyreuther.png",
  },
  "untitled-by-klaus-lieber": {
    slug: "untitled-by-klaus-lieber",
    name: "Untitled",
    author: "Klaus Lieber",
    year: 1983,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/untitled-klaus-lieber.png",
  },
  "untitled-by-nicolai-dolmatov": {
    slug: "untitled-by-nicolai-dolmatov",
    name: "Untitled",
    author: "Nicolai Dolmatov",
    year: 1995,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/untitled-nicolai-dolmatov.png",
  },
  "untitled-by-robert-stuckey": {
    slug: "untitled-by-robert-stuckey",
    name: "Untitled",
    author: "Robert Stuckey",
    year: 1983,
    chromaticScaleImagePath:
      "/wp-content/uploads/2012/07/untitled-robert-stuckey.png",
  },
};
